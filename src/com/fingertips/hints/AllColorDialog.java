package com.fingertips.hints;

import java.util.ArrayList;

import com.fingertips.hinthunt.R;
import com.fingertips.canvas.CanvasActivity;
import com.fingertips.canvas.Colore;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class AllColorDialog extends Dialog {

	private final static String MY_PREFERENCES = "preferences";

	Colore Color0 = new Colore("disponibile", Color.rgb(156, 118, 81));
	Colore Color1 = new Colore("disponibile", Color.rgb(51, 161, 162));
	Colore Color2 = new Colore("disponibile", Color.rgb(250, 104, 0));

	Colore Color3 = new Colore("disponibile", Color.rgb(234, 117, 84));
	Colore Color4 = new Colore("disponibile", Color.rgb(180, 149, 22));
	Colore Color5 = new Colore("disponibile", Color.rgb(53, 192, 233));

	Colore Color6 = new Colore("non_disponibile", Color.rgb(233, 169, 81));
	Colore Color7 = new Colore("disponibile", Color.rgb(217, 195, 172));
	Colore Color8 = new Colore("non_disponibile", Color.rgb(3, 30, 85));

	Colore Color9 = new Colore("disponibile", Color.rgb(202, 98, 97));
	Colore Color10 = new Colore("disponibile", Color.rgb(94, 135, 91));
	Colore Color11 = new Colore("disponibile", Color.rgb(175, 184, 191));

	Colore Color12 = new Colore("disponibile", Color.rgb(65, 47, 33));
	Colore Color13 = new Colore("disponibile", Color.rgb(247, 223, 199));
	Colore Color14 = new Colore("disponibile", Color.rgb(224, 179, 88));

	Colore Color15 = new Colore("disponibile", Color.rgb(109, 23, 36));
	Colore Color16 = new Colore("disponibile", Color.rgb(249, 219, 107));
	Colore Color17 = new Colore("disponibile", Color.rgb(220, 201, 168));

	Colore Color18 = new Colore("disponibile", Color.rgb(138, 134, 97));
	Colore Color19 = new Colore("disponibile", Color.rgb(179, 199, 212));
	Colore Color20 = new Colore("disponibile", Color.rgb(239, 150, 120));

	Colore Color21 = new Colore("disponibile", Color.rgb(51, 59, 46));
	Colore Color22 = new Colore("disponibile", Color.rgb(228, 201, 60));
	Colore Color23 = new Colore("disponibile", Color.rgb(78, 81, 96));

	public ArrayList<Colore> allColors = new ArrayList<Colore>();
	String stanza;
	Context context;

	public AllColorDialog(Context context, String stanza) {
		super(context);
		this.context = context;
		this.stanza = stanza;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setTitle(context.getResources().getString(R.string.colorPicker1));
		setContentView(R.layout.allcolor_dialog);

		allColors.add(Color0);
		allColors.add(Color1);
		allColors.add(Color2);
		allColors.add(Color3);
		allColors.add(Color4);
		allColors.add(Color5);
		allColors.add(Color6);
		allColors.add(Color7);
		allColors.add(Color8);
		allColors.add(Color9);

		allColors.add(Color10);
		allColors.add(Color11);
		allColors.add(Color12);
		allColors.add(Color13);
		allColors.add(Color14);
		allColors.add(Color15);
		allColors.add(Color16);
		allColors.add(Color17);
		allColors.add(Color18);
		allColors.add(Color19);

		allColors.add(Color20);
		allColors.add(Color21);
		allColors.add(Color22);
		allColors.add(Color23);

		Button[] AllButtonColors = new Button[] {
				(Button) findViewById(R.id.Button1_1),
				(Button) findViewById(R.id.Button1_2),
				(Button) findViewById(R.id.Button1_3),

				(Button) findViewById(R.id.Button2_1),
				(Button) findViewById(R.id.Button2_2),
				(Button) findViewById(R.id.Button2_3),

				(Button) findViewById(R.id.Button3_1),
				(Button) findViewById(R.id.Button3_2),
				(Button) findViewById(R.id.Button3_3),

				(Button) findViewById(R.id.Button4_1),
				(Button) findViewById(R.id.Button4_2),
				(Button) findViewById(R.id.Button4_3),

				(Button) findViewById(R.id.Button5_1),
				(Button) findViewById(R.id.Button5_2),
				(Button) findViewById(R.id.Button5_3),

				(Button) findViewById(R.id.button6_1),
				(Button) findViewById(R.id.button6_2),
				(Button) findViewById(R.id.button6_3),

				(Button) findViewById(R.id.button7_1),
				(Button) findViewById(R.id.button7_2),
				(Button) findViewById(R.id.button7_3),

				(Button) findViewById(R.id.button8_1),
				(Button) findViewById(R.id.button8_2),
				(Button) findViewById(R.id.button8_3) };

		View.OnClickListener l = new View.OnClickListener() {
			public void onClick(final View v) {
				DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						switch (which) {
						case DialogInterface.BUTTON_POSITIVE:
							Button b = (Button) v;

							SharedPreferences prefs = getContext()
									.getSharedPreferences(MY_PREFERENCES,
											Context.MODE_PRIVATE);

							boolean valid = true;

							// c � il codice del colore selezionato attualmente
							int c = b.getCurrentTextColor();

							// controllo se c'� gi� nella tavolozza dell'utente
							for (int x = 0; x < 6; x++) {
								int color = prefs.getInt("coloreScelto" + x, 0);
								if (c == color) {
									valid = false;
								}
							}

							if (valid) {

								int pos = prefs.getInt("posizione", 0);

								SharedPreferences.Editor editor = prefs.edit();
								editor.putInt("coloreScelto" + pos, c);
								editor.putBoolean("FinitoStanza" + stanza, true);

								editor.commit();

								String msg = context.getResources().getString(
										R.string.allColor1);

								Toast toast = Toast.makeText(getContext(), msg,
										Toast.LENGTH_LONG);
								toast.show();

								dismiss();

							}

							else {
								Toast toast = Toast.makeText(
										getContext(),
										context.getResources().getString(
												R.string.allColor2),
										Toast.LENGTH_SHORT);
								toast.show();
								return;

							}

							Intent i = new Intent(getContext(),
									CanvasActivity.class);

							getContext().startActivity(i);
							((Activity) context).finish();
							break;

						case DialogInterface.BUTTON_NEGATIVE:
							// No button clicked

							dialog.dismiss();
							break;
						}
					}

				};
				Builder builder = new AlertDialog.Builder(getContext());
				builder.setMessage(
						context.getResources().getString(R.string.sicuro))
						.setPositiveButton(
								context.getResources().getString(R.string.si),
								dialogClickListener)
						.setNegativeButton(
								context.getResources().getString(R.string.no),
								dialogClickListener).show();

			}
		};

		for (int x = 0; x < AllButtonColors.length; x++) {

			AllButtonColors[x].setBackgroundColor(allColors.get(x).getCodice());
			AllButtonColors[x].setTextColor(allColors.get(x).getCodice());

			AllButtonColors[x]
					.setOnClickListener((android.view.View.OnClickListener) l);

		}

	}

}
