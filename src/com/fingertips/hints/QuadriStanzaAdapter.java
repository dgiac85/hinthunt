package com.fingertips.hints;

import java.util.ArrayList;

import com.fingertips.hinthunt.R;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class QuadriStanzaAdapter extends BaseAdapter {
	private Context mContext;
	private LayoutInflater layoutInflater;
	ArrayList<Quadro> itemList = new ArrayList<Quadro>();

	public QuadriStanzaAdapter(QuadriStanza activity) {
		layoutInflater = (LayoutInflater) activity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mContext = activity.getApplicationContext();
	}

	void add(Quadro path) {
		itemList.add(path);
	}

	public int getCount() {
		return itemList.size();
	}

	public Quadro getItem(int arg0) {
		// TODO Auto-generated method stub
		return itemList.get(arg0);
	}

	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;

		if (rowView == null) {
			rowView = layoutInflater.inflate(R.layout.opere, null);
		}
		String fontPath = "fonts/Noteworthy.ttc";
		Typeface tf = Typeface.createFromAsset(mContext.getResources()
				.getAssets(), fontPath);
		TextView textView = (TextView) rowView.findViewById(R.id.label);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.logo);
		textView.setTypeface(tf);
		textView.setText(itemList.get(position).getOpera());
		imageView.setBackgroundResource(itemList.get(position).getDrawable());

		return rowView;
	}
}
