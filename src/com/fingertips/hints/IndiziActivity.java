package com.fingertips.hints;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fingertips.hinthunt.R;
import com.fingertips.canvas.CanvasActivity;
import com.fingertips.utils.ReadJSON;


import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterViewFlipper;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class IndiziActivity extends Activity {
	
	JSONArray arrayJSON=null;
	Quadro m;
	boolean controllo=true;
	String numStanza;
	List<String> indizi=new ArrayList<String>();
	int i=1;
	int totProvvisorioIndizi=0;
	int numeroIndiziLetti;
	int numeroSerieIndizi;
	int varAppoggioIndizi;
	String lingua=null;
	Button prev, next, laso;
	ImageView imgTutorial;
	AnimationDrawable tutorial;
	private final static String MY_PREFERENCES = "preferences";
	ReadJSON obj=new ReadJSON();
	TextView text, nstanza;
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	  super.onConfigurationChanged(newConfig);
	  
	    // Checks the orientation of the screen
	    if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
	    	setContentView(R.layout.activity_indizi);
	    } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
	    	setContentView(R.layout.activity_indizi);
	    }

	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_indizi);
		lingua = Locale.getDefault().getDisplayLanguage();
		String fontPath = "fonts/Noteworthy.ttc";
        
        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        
        prev=(Button)findViewById(R.id.prev);
	    prev.setTypeface(tf);
	    
	    next=(Button)findViewById(R.id.next);
	    next.setTypeface(tf);
	    
	    laso=(Button)findViewById(R.id.laso);
	    laso.setTypeface(tf);
	    
	    nstanza=(TextView)findViewById(R.id.numstanza);
	    nstanza.setTypeface(tf);
	    
	    text=(TextView)findViewById(R.id.textNumIndizi);
	    text.setTypeface(tf);
	   

		SharedPreferences settings = getSharedPreferences(MY_PREFERENCES, 0);
		SharedPreferences.Editor editor = settings.edit();		 
		
        Intent intent = getIntent();
        numStanza = intent.getStringExtra("position");
        nstanza = (TextView)findViewById(R.id.numstanza);
        nstanza.setText(getApplicationContext().getResources().getString(R.string.Stanza)+numStanza);
        
        //tutorial
	    imgTutorial = (ImageView) findViewById(R.id.img_tutorial);
	    tutorial = (AnimationDrawable) imgTutorial.getBackground();
	    
	    if(settings.getInt("INDIZI", 0)<3){
	    tutorial.start();
	    laso.setEnabled(false);
	    prev.setEnabled(false);
	    next.setEnabled(false);
	    }
	    
	    else{
	    	tutorial.stop();
         	imgTutorial.setVisibility(View.GONE);
	    }
        
        m=new Quadro();        
        System.out.println("ARRIVAQUI!!");
        text=(TextView) findViewById(R.id.textNumIndizi);
        if (settings.getInt("primavolta",0)==0){
        	editor.putBoolean("controllo",true);
        	editor.commit();        	
        }
       
        //all'on create alla prima volta segnare la serie di indizi vista e continuare con quella richiamando dalle shared
        if (!(settings.contains("QuadroStanza"+numStanza))){         	
        		
	        try {
	        	totProvvisorioIndizi=settings.getInt("INDIZI", 0);
	            totProvvisorioIndizi+=1;
	            editor.putInt("INDIZI",totProvvisorioIndizi);
	            editor.commit(); 
	            text.setText(getApplicationContext().getResources().getString(R.string.photo2) + ":" +settings.getInt("INDIZI", 0));
	            if (lingua.equalsIgnoreCase("Italiano")) {
					arrayJSON = obj.leggiJSON(this, "stanze/stanza" + numStanza
							+ "/quadri.json");
				}
				if (lingua.equalsIgnoreCase("English")) {
					arrayJSON = obj.leggiJSON(this, "stanze/stanza" + numStanza
							+ "/quadri_en.json");
				}
				Random randomGenerator = new Random();
				//ottengo un'opera casualmente
				int randomQuadro = randomGenerator.nextInt(arrayJSON.length());
				
	            JSONObject objectJSON = arrayJSON.getJSONObject(randomQuadro);
	            m.setId(objectJSON.getInt("id")); 
	            m.setOpera(null);
	            //m.setPaththumb(null);
	            
	            numeroSerieIndizi=objectJSON.getInt("numSerie");
	            int randomZ=randomGenerator.nextInt(numeroSerieIndizi);
	            
	            
	            editor.putInt("SerieStanza"+randomQuadro, randomZ);
	            editor.putInt("QuadroStanza"+numStanza, randomQuadro);
	            editor.putInt("NumeroIndiziLetti"+numStanza,1);
	            editor.putInt("IndiziUtilizzati",0);
				editor.commit(); 
				
				numeroIndiziLetti=settings.getInt("NumeroIndiziLetti"+numStanza,1);
				
	            for (int z1=(randomZ*5+1); z1<=(randomZ*5+5);z1++)
	            	indizi.add(objectJSON.getString("indizio"+z1));
	            //ottengo dinamicamente gli indizi senza preoccuparmi di quanti sono
	            /*while (controllo){
	            	try{
	            	//if(objectJSON.getString("indizio"+z))
	        		indizi.add(objectJSON.getString("indizio"+randomZ));
	        		randomZ++;
	        		System.out.println("lofa");
	            	}
	            	catch(RuntimeException e)
	            	{controllo=false;}                	
	            		
	            }*/
	            m.setIndizi(indizi);           	
	            
		}
        catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
        }
        else{
        	totProvvisorioIndizi=settings.getInt("INDIZI", 0);  	
        	text.setText(getApplicationContext().getResources().getString(R.string.photo2) + ":" +settings.getInt("INDIZI", 0));
        	try {
        		numeroIndiziLetti=settings.getInt("NumeroIndiziLetti"+numStanza,1);
        		if (lingua.equalsIgnoreCase("Italiano")) {
					arrayJSON = obj.leggiJSON(this, "stanze/stanza" + numStanza
							+ "/quadri.json");
				}
				if (lingua.equalsIgnoreCase("English")) {
					arrayJSON = obj.leggiJSON(this, "stanze/stanza" + numStanza
							+ "/quadri_en.json");
				}				
				JSONObject objectJSON = arrayJSON.getJSONObject(settings.getInt("QuadroStanza"+numStanza, 0));
	            m.setId(objectJSON.getInt("id")); 
	            m.setOpera(null);
	            //m.setPaththumb(null);
	            int Z=settings.getInt("SerieStanza"+settings.getInt("QuadroStanza"+numStanza, 0), 0);
	            for (int z1=(Z*5+1); z1<=(Z*5+5);z1++)
	            	indizi.add(objectJSON.getString("indizio"+z1));
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
        }
        
        final Button next = (Button)findViewById(R.id.next);
        final Button prev = (Button)findViewById(R.id.prev);
        Button laso = (Button)findViewById(R.id.laso);
        //final TextView indizio = (TextView)findViewById(R.id.indizio);
        //indizio.setText(indizi.get(0).toString());
        System.out.println("arriva qui");
        
        final AdapterViewFlipper calView = (AdapterViewFlipper) findViewById(R.id.adapterViewFlipper1);
        calView.setAdapter(new IndiziAdapter(this,R.layout.activity_indizi, indizi));
       System.out.println("NUMEROINDIZILETTI="+numeroIndiziLetti);
       i=1;
        next.setOnClickListener(new View.OnClickListener() {
			
        	
			public void onClick(View v) {
				
			    
				prev.setEnabled(true);
				//indizio.setText(indizi.get(i).toString());
				
				
				
			 	if (i>=numeroIndiziLetti){
			 		  DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
						    public void onClick(DialogInterface dialog, int which) {
						    	SharedPreferences settings = getSharedPreferences(MY_PREFERENCES, 0);
								SharedPreferences.Editor editor = settings.edit();
						        switch (which){
						        case DialogInterface.BUTTON_POSITIVE:		
						        	calView.showNext();						        									
						        	i+=1;	
						        	if (i==indizi.size())				
										next.setEnabled(false);		
						        	System.out.println("AUMENTA I="+i);
						        	varAppoggioIndizi=settings.getInt("IndiziUtilizzati", 0);
						        	varAppoggioIndizi+=1;
						        	editor.putInt("IndiziUtilizzati", varAppoggioIndizi);
						        	editor.putInt("NumeroIndiziLetti"+numStanza,i);
						        	totProvvisorioIndizi=settings.getInt("INDIZI", 0);
						            totProvvisorioIndizi+=1;
						            editor.putInt("INDIZI",totProvvisorioIndizi);
						        	editor.commit();
						        	text.setText(getApplicationContext().getResources().getString(R.string.photo2) + ":" +settings.getInt("INDIZI", 0));  
						        	numeroIndiziLetti=settings.getInt("NumeroIndiziLetti"+numStanza, 1);
						        	System.out.println("NUMEROINDIZILETTI="+numeroIndiziLetti);
						            break;

						        case DialogInterface.BUTTON_NEGATIVE:
						            //No button clicked
						        	if (i==1){
						        		prev.setEnabled(false);
						        	}
						        	dialog.dismiss();
						            break;
						        }
						    }
						};

						Builder builder = new AlertDialog.Builder(IndiziActivity.this);
						builder.setMessage("Sei Sicuro di voler passare al Prossimo Indizio!?!").setPositiveButton("Yes", dialogClickListener)
						    .setNegativeButton("No", dialogClickListener).show();
			 	}
			 	else{
			 		calView.showNext();
			 		i+=1;
			 		if (i==indizi.size())				
						next.setEnabled(false);		
			 	}
								
			}
		});
		
        prev.setEnabled(false);
        prev.setOnClickListener(new View.OnClickListener() {
			
        		
			public void onClick(View v) {
				// TODO Auto-generated method stub
				i-=1;
				System.out.println("DIMINUISCE I="+i);
				calView.showPrevious();
				next.setEnabled(true);
				//indizio.setText(indizi.get(i).toString());
				if (i==1){				
					prev.setEnabled(false);
					
				}
				
				
				
			}
		});
        
        laso.setOnClickListener(new View.OnClickListener() {
			
        	public void onClick(View v) {
				// TODO Auto-generated method stub				
        		SharedPreferences settings = getSharedPreferences(MY_PREFERENCES, 0);
	    		SharedPreferences.Editor editor = settings.edit();
			    Intent i = new Intent(getApplicationContext(), QuadriStanza.class);		 	             
	                i.putExtra("soluzione", m.getId());
	                i.putExtra("position", numStanza);
	                editor.putBoolean("laso"+numStanza,true);
	                editor.commit();
				startActivity(i);
 	            IndiziActivity.this.finish();			
 	             			
			}
		});
       
		
	}
	

	
	
	
	
	 @Override
		protected void onRestart()
		{
		super.onRestart();
		Log.i(this.getClass().toString(), "Richiamato onRestart()"); }
	    
		@Override
		protected void onStart() {
		super.onStart();
		Log.i(this.getClass().toString(), "Richiamato onStart()"); }
		
		@Override
		protected void onResume() {
		super.onResume();
		
		Log.i(this.getClass().toString(), "Richiamato onResume()");
		
	
		}
		
		@Override
		public boolean onKeyDown(int keyCode, KeyEvent event)
		{
		    if(keyCode == KeyEvent.KEYCODE_BACK)
		    {
		        System.out.println("Test-Back button pressed!");
		    }
		    else if(keyCode == KeyEvent.KEYCODE_HOME)
		    {
		    	
		    }
		    return super.onKeyDown(keyCode, event);
		}
		
		
		
		@Override
		public void onBackPressed() {
//			DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
//			    public void onClick(DialogInterface dialog, int which) {
//			        switch (which){
//			        case DialogInterface.BUTTON_POSITIVE:				            
						
					    Intent i = new Intent(getApplicationContext(), CanvasActivity.class);		 	             
						startActivity(i);
						
		 	            IndiziActivity.this.finish();
		 	           
//			            break;
//
//			        case DialogInterface.BUTTON_NEGATIVE:
//			            //No button clicked				        	
//			        	dialog.dismiss();
//			            break;
//			        }
//			    }
//			};

//			Builder builder = new AlertDialog.Builder(IndiziActivity.this);
//			builder.setMessage("Sei Sicuro!?!").setPositiveButton("Yes", dialogClickListener)
//			    .setNegativeButton("No", dialogClickListener).show();
		}
		@Override
		protected void onStop() {
			super.onStop();
			Log.i(this.getClass().toString(), "Richiamato onStop()");

			/*DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			    @Override
			    public void onClick(DialogInterface dialog, int which) {
			        switch (which){
			        case DialogInterface.BUTTON_POSITIVE:				            
						
					    Intent i = new Intent(getApplicationContext(), MainActivity.class);		 	             
						startActivity(i);
						
		 	            IndiziActivity.this.finish();
		 	           
			            break;

			        case DialogInterface.BUTTON_NEGATIVE:
			            //No button clicked				        	
			        	dialog.dismiss();
			            break;
			        }
			    }
			};

			Builder builder = new AlertDialog.Builder(IndiziActivity.this);
			builder.setMessage("Sei Sicuro!?!").setPositiveButton("Yes", dialogClickListener)
			    .setNegativeButton("No", dialogClickListener).show();*/
			
			
		}
		
		@Override
		protected void onDestroy() {
		super.onDestroy();
		
			
		Log.i(this.getClass().toString(), "Richiamato onDestroy()");
		}
		
		@Override
		protected void onPause() {
			super.onPause();
			//IndiziActivity.this.finish();		
			Log.i(this.getClass().toString(), "Richiamato onPause()"); }
		
		 @Override
         public boolean onTouchEvent(MotionEvent event) {

	         	

	             switch (event.getAction()) {
	                 case MotionEvent.ACTION_DOWN:
	                 	
	                 	tutorial.stop();
	                 	laso.setEnabled(true);
	            	    prev.setEnabled(true);
	            	    next.setEnabled(true);
	                 	imgTutorial.setVisibility(View.GONE);
	                 		
	                     break;
	                     	             
	             }
	           
	             return true;
		 }
		
		 
		

}
