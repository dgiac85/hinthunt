package com.fingertips.hints;

import java.util.ArrayList;
import java.util.List;

//i dati relativi al quadro saranno raccolti tramite file JSON.

public class Quadro {

	private int id;
	private String opera;

	private int drawable;
	private List<String> indizi = new ArrayList<String>();
	private String dettagli;

	// nel caso si dovesse usare un'istanza di tipo singleton
	// private static Quadro istanza;

	public Quadro() {

	}

	public Quadro(int id, String opera, int drawable, List<String> indizi,
			String dettagli) {
		super();
		this.id = id;
		this.opera = opera;
		this.dettagli = dettagli;
		this.drawable = drawable;
		this.indizi = indizi;

	}

	public int getId() {
		return id;
	}

	public void setId(int i) {
		this.id = i;
	}

	public String getOpera() {
		return opera;
	}

	public void setOpera(String opera) {
		this.opera = opera;
	}

	public List<String> getIndizi() {
		return indizi;
	}

	public void setIndizi(List<String> indizi) {
		this.indizi = indizi;
	}

	public int getDrawable() {
		return drawable;
	}

	public void setDrawable(int drawable) {
		this.drawable = drawable;
	}

	public void setDettagli(String dettagli) {
		this.dettagli = dettagli;

	}

	public String getDettagli() {
		return dettagli;
	}

}
