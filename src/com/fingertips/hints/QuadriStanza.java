package com.fingertips.hints;

import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fingertips.hinthunt.R;
import com.fingertips.utils.ReadJSON;

import android.os.Bundle;
import android.animation.Animator;

import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.Html;
import android.text.Spanned;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class QuadriStanza extends Activity {

	private Animator mCurrentAnimator;
	private int mShortAnimationDuration;
	JSONArray arrayJSON = null;
	Typeface tf;
	String numStanza;
	ListView lv;

	TextView lista;
	Button torna;

	int soluzione;
	int immagine;
	final Rect startBounds = new Rect();
	final Rect finalBounds = new Rect();
	final Point globalOffset = new Point();
	ImageView expandedImageView;
	float startScaleFinal;
	View thumbView;
	private final static String MY_PREFERENCES = "preferences";
	String lingua=null;
	Button b1;
	Button rispondi;
	Button chiudi;
	Button info;

	@Override
	public void onBackPressed() {
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
				case DialogInterface.BUTTON_POSITIVE:

					QuadriStanza.this.finish();
					break;

				case DialogInterface.BUTTON_NEGATIVE:
					// No button clicked
					dialog.dismiss();
					break;
				}
			}
		};

		Builder builder = new AlertDialog.Builder(QuadriStanza.this);
		builder.setMessage(
				getApplicationContext().getResources().getString(
						R.string.sicuro3))
				.setPositiveButton(
						getApplicationContext().getResources().getString(
								R.string.si), dialogClickListener)
				.setNegativeButton(
						getApplicationContext().getResources().getString(
								R.string.no), dialogClickListener).show();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		// Checks the orientation of the screen
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			setContentView(R.layout.activity_quadri_stanza);
		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			setContentView(R.layout.activity_quadri_stanza);
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_quadri_stanza);
		String fontPath = "fonts/Noteworthy.ttc";
		tf = Typeface.createFromAsset(getAssets(), fontPath);
		lingua = Locale.getDefault().getDisplayLanguage();
		ReadJSON obj = new ReadJSON();
		Intent intent = getIntent();

		lista = (TextView) findViewById(R.id.textNumIndizi);
		lista.setTypeface(tf);

		torna = (Button) findViewById(R.id.but1);
		torna.setTypeface(tf);

		rispondi = (Button) findViewById(R.id.padd1);
		rispondi.setTypeface(tf);

		chiudi = (Button) findViewById(R.id.padd2);
		chiudi.setTypeface(tf);

		info = (Button) findViewById(R.id.padd3);
		info.setTypeface(tf);

		SharedPreferences settings = getSharedPreferences(MY_PREFERENCES, 0);
		final SharedPreferences.Editor editor = settings.edit();
		soluzione = intent.getIntExtra("soluzione", 0);
		mShortAnimationDuration = getResources().getInteger(
				android.R.integer.config_shortAnimTime);

		chiudi.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				chiusura();
			}
		});

		QuadriStanzaAdapter qsa = new QuadriStanzaAdapter(this);
		lv = (ListView) findViewById(R.id.list);
		lv.setAdapter(qsa);

		numStanza = intent.getStringExtra("position");

		try {
			if (lingua.equalsIgnoreCase("Italiano")) {
				arrayJSON = obj.leggiJSON(this, "stanze/stanza" + numStanza
						+ "/quadri.json");
			}
			if (lingua.equalsIgnoreCase("English")) {
				arrayJSON = obj.leggiJSON(this, "stanze/stanza" + numStanza
						+ "/quadri_en.json");
			}
			for (int i = 0; i < arrayJSON.length(); i++) {
				JSONObject objectJSON = arrayJSON.getJSONObject(i);
				int id = objectJSON.getInt("id");
				String uri = objectJSON.getString("thumb");
				String opera = objectJSON.getString("opera");
				int immagine = getResources().getIdentifier(uri, null,
						getPackageName());

				qsa.add(new Quadro(id, opera, immagine, null, null));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		torna.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(),
						IndiziActivity.class);
				i.putExtra("position", numStanza);
				startActivity(i);
				QuadriStanza.this.finish();
			}
		});

		// listening to single list item on click
		lv.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				lv.setEnabled(false);
				final int posi = position;
				final int pos = position;

				rispondi.setVisibility(View.VISIBLE);
				chiudi.setVisibility(View.VISIBLE);
				info.setVisibility(View.VISIBLE);

				zoomImageFromThumb(view, pos);
				// sending data to new activity

				info.setOnClickListener(new View.OnClickListener() {
					public void onClick(View view) {
						mostraDialogInfo(pos);
					}
				});

				rispondi.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								int pos = posi;
								pos += 1;
								switch (which) {
								case DialogInterface.BUTTON_POSITIVE:
									// salvare un boolean nelle shared che se
									// torna=false l'intent lo fa ritornare al
									// quadro!

									Intent i = new Intent(
											getApplicationContext(),
											SceltaColoreOrError.class);
									if (pos == soluzione) {
										i.putExtra("trovato", true);
										i.putExtra("numStanza", numStanza);
										editor.putBoolean(
												"trovato" + numStanza, true);
										editor.putBoolean(
												"risolto" + numStanza, true);
										editor.commit();
										// salva nelle shared che la stanza
										// numero qualcosa � stata risolta
									} else {
										i.putExtra("trovato", false);
										i.putExtra("numStanza", numStanza);
										editor.putBoolean(
												"trovato" + numStanza, false);
										editor.putBoolean(
												"risolto" + numStanza, true);
										editor.commit();
									}

									startActivity(i);

									QuadriStanza.this.finish();

									break;

								case DialogInterface.BUTTON_NEGATIVE:
									// No button clicked
									dialog.dismiss();
									break;
								}
							}
						};

						Builder builder = new AlertDialog.Builder(
								QuadriStanza.this);
						builder.setMessage(
								getApplicationContext().getResources()
										.getString(R.string.sicuro))
								.setPositiveButton(
										getApplicationContext().getResources()
												.getString(R.string.si),
										dialogClickListener)
								.setNegativeButton(
										getApplicationContext().getResources()
												.getString(R.string.no),
										dialogClickListener).show();

					}
				});
			}// chiusura pad1 onclick
		});// chiusurapad1 listener
	}

	private void zoomImageFromThumb(final View thumbView, int imageResId) {
		// If there's an animation in progress, cancel it immediately and
		// proceed with this one.
		if (mCurrentAnimator != null) {
			mCurrentAnimator.cancel();
		}

		// Load the high-resolution "zoomed-in" image.
		expandedImageView = (ImageView) findViewById(R.id.expanded_image1);

		try {
			for (int i = 0; i < arrayJSON.length(); i++) {
				JSONObject objectJSON = arrayJSON.getJSONObject(i);
				String uri = objectJSON.getString("drawable");
				if (i == imageResId) {
					immagine = getResources().getIdentifier(uri, null,
							getPackageName());

				}

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		expandedImageView.setImageResource(immagine);
		// Calculate the starting and ending bounds for the zoomed-in image.
		// This step
		// involves lots of math. Yay, math.

		// The start bounds are the global visible rectangle of the thumbnail,
		// and the
		// final bounds are the global visible rectangle of the container view.
		// Also
		// set the container view's offset as the origin for the bounds, since
		// that's
		// the origin for the positioning animation properties (X, Y).
		thumbView.getGlobalVisibleRect(startBounds);
		findViewById(R.id.container1).getGlobalVisibleRect(finalBounds,
				globalOffset);
		startBounds.offset(-globalOffset.x, -globalOffset.y);
		finalBounds.offset(-globalOffset.x, -globalOffset.y);

		// Adjust the start bounds to be the same aspect ratio as the final
		// bounds using the
		// "center crop" technique. This prevents undesirable stretching during
		// the animation.
		// Also calculate the start scaling factor (the end scaling factor is
		// always 1.0).
		float startScale;
		if ((float) finalBounds.width() / finalBounds.height() > (float) startBounds
				.width() / startBounds.height()) {
			// Extend start bounds horizontally
			startScale = (float) startBounds.height() / finalBounds.height();
			float startWidth = startScale * finalBounds.width();
			float deltaWidth = (startWidth - startBounds.width()) / 2;
			startBounds.left -= deltaWidth;
			startBounds.right += deltaWidth;
		} else {
			// Extend start bounds vertically
			startScale = (float) startBounds.width() / finalBounds.width();
			float startHeight = startScale * finalBounds.height();
			float deltaHeight = (startHeight - startBounds.height()) / 2;
			startBounds.top -= deltaHeight;
			startBounds.bottom += deltaHeight;
		}

		// Hide the thumbnail and show the zoomed-in view. When the animation
		// begins,
		// it will position the zoomed-in view in the place of the thumbnail.
		// thumbView.setAlpha(0f);
		expandedImageView.setVisibility(View.VISIBLE);

		// Set the pivot point for SCALE_X and SCALE_Y transformations to the
		// top-left corner of
		// the zoomed-in view (the default is the center of the view).
		expandedImageView.setPivotX(0f);
		expandedImageView.setPivotY(0f);

		// Construct and run the parallel animation of the four translation and
		// scale properties
		// (X, Y, SCALE_X, and SCALE_Y).

		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		System.out.println(currentapiVersion);
		if (currentapiVersion > android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			// grid.setAlpha(0.1f);
			expandedImageView.setBackgroundColor(Color.argb(127, 0, 0, 0));
			// expandedImageView.setAlpha(1f);
			// Do something for froyo and above versions
			AnimatorSet set = new AnimatorSet();
			set.play(
					ObjectAnimator.ofFloat(expandedImageView, View.X,
							startBounds.left, finalBounds.left))
					.with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
							startBounds.top, finalBounds.top))
					.with(ObjectAnimator.ofFloat(expandedImageView,
							View.SCALE_X, startScale, 1f))
					.with(ObjectAnimator.ofFloat(expandedImageView,
							View.SCALE_Y, startScale, 1f))
					.with(ObjectAnimator.ofFloat(expandedImageView,
							expandedImageView.ALPHA, 0, 1));
			set.setDuration(mShortAnimationDuration);
			set.setInterpolator(new DecelerateInterpolator());
			set.addListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mCurrentAnimator = null;
					lista.setVisibility(View.VISIBLE);
				}

				@Override
				public void onAnimationCancel(Animator animation) {
					mCurrentAnimator = null;
					lista.setVisibility(View.VISIBLE);
				}
			});
			set.start();
			mCurrentAnimator = set;
		} else {
			System.out.println("non fare niente");
			// do something for phones running an SDK before froyo
		}

		// Upon clicking the zoomed-in image, it should zoom back down to the
		// original bounds
		// and show the thumbnail instead of the expanded image.
		expandedImageView.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				chiusura();

			}
		});
	}

	public void mostraDialogInfo(int premuto) {
		final Dialog dialogInfo = new Dialog(QuadriStanza.this);
		dialogInfo.setTitle(R.string.infoQuadro);
		int num = premuto;
		dialogInfo.setContentView(R.layout.info_quadri);
		Button exit = (Button) dialogInfo.getWindow().findViewById(
				R.id.closeInfo);
		TextView annot = (TextView) dialogInfo.getWindow().findViewById(
				R.id.anno);
		TextView titolot = (TextView) dialogInfo.getWindow().findViewById(
				R.id.titolo);
		TextView tecnicat = (TextView) dialogInfo.getWindow().findViewById(
				R.id.tecnica);
		TextView stanzat = (TextView) dialogInfo.getWindow().findViewById(
				R.id.stanza);
		annot.setTypeface(tf);
		titolot.setTypeface(tf);
		tecnicat.setTypeface(tf);
		stanzat.setTypeface(tf);
		exit.setTypeface(tf);
		ImageView imageInfo = (ImageView) dialogInfo.getWindow().findViewById(
				R.id.imageInfo);
		dialogInfo.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

		ReadJSON obj = new ReadJSON();

		try {
			if (lingua.equalsIgnoreCase("Italiano")) {
				arrayJSON = obj.leggiJSON(this, "stanze/stanza" + numStanza	+ "/quadri.json");
			}
			if (lingua.equalsIgnoreCase("English")) {
				arrayJSON = obj.leggiJSON(this, "stanze/stanza" + numStanza	+ "/quadri_en.json");
			}

			for (int i = 0; i < arrayJSON.length(); i++) {
				JSONObject objectJSON = arrayJSON.getJSONObject(i);
				if (i == num) {
					String immagine = objectJSON.getString("thumb");
					String anno = objectJSON.getString("anno");
					String titolo = objectJSON.getString("opera");
					String tecnica = objectJSON.getString("tecnica");
					String stanza = objectJSON.getString("stanza");
					Spanned spannable1 = Html.fromHtml(anno);
					Spanned spannable2 = Html.fromHtml(titolo);
					Spanned spannable3 = Html.fromHtml(tecnica);
					Spanned spannable4 = Html.fromHtml(stanza);

					annot.setText(getApplicationContext().getResources().getString(R.string.Anno) + ":" + spannable1);
					titolot.setText(spannable2);
					stanzat.setText(getApplicationContext().getResources().getString(R.string.Tecnica) + ": " + spannable3);
					tecnicat.setText(getApplicationContext().getResources().getString(R.string.Stanza) + ":" + spannable4);

					// settare l'immagine vista all'interno della dialog del
					// quadro scelto
					int intImmagine = getResources().getIdentifier(immagine,
							null, getPackageName());
					imageInfo.setImageResource(intImmagine);

			}}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		exit.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				dialogInfo.dismiss();
			}
		});

		dialogInfo.show();
	}

	void chiusura() {
		lv.setEnabled(true);
		rispondi.setVisibility(View.INVISIBLE);
		chiudi.setVisibility(View.INVISIBLE);
		info.setVisibility(View.INVISIBLE);

		if (mCurrentAnimator != null) {
			mCurrentAnimator.cancel();

		}

		// Animate the four positioning/sizing properties in parallel,
		// back to their
		// original values.
		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		if (currentapiVersion > android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			AnimatorSet set = new AnimatorSet();
			set.play(
					ObjectAnimator.ofFloat(expandedImageView, View.X,
							startBounds.left))
					.with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
							startBounds.top))
					.with(ObjectAnimator.ofFloat(expandedImageView,
							View.SCALE_X, startScaleFinal))
					.with(ObjectAnimator.ofFloat(expandedImageView,
							View.SCALE_Y, startScaleFinal))
					.with(ObjectAnimator.ofFloat(expandedImageView, View.ALPHA,
							1, 0));
			set.setDuration(mShortAnimationDuration);
			set.setInterpolator(new DecelerateInterpolator());
			set.addListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {

					expandedImageView.setVisibility(View.GONE);
					mCurrentAnimator = null;
				}

				@Override
				public void onAnimationCancel(Animator animation) {

					expandedImageView.setVisibility(View.GONE);

					mCurrentAnimator = null;
				}
			});
			set.start();
			mCurrentAnimator = set;
		} else {
			thumbView.setAlpha(1f);
			expandedImageView.setVisibility(View.GONE);
			System.out.println("non fare di nuovo niente");
		}
	}
}
