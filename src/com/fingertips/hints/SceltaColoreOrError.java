package com.fingertips.hints;

import com.fingertips.hinthunt.R;
import com.fingertips.canvas.CanvasActivity;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class SceltaColoreOrError extends Activity {

	private final static String MY_PREFERENCES = "preferences";
	String numStanza;
	boolean trovato;
	String stanza;
	TextView esito;
	Button clear;
	ImageView risultato;

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		// Checks the orientation of the screen
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			setContentView(R.layout.activity_scelta_quadro);
		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			setContentView(R.layout.activity_scelta_quadro);
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scelta_quadro);
		SharedPreferences settings = getSharedPreferences(MY_PREFERENCES,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();

		String fontPath = "fonts/Noteworthy.ttc";
		Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);

		esito = (TextView) findViewById(R.id.esito);
		esito.setTypeface(tf);

		clear = (Button) findViewById(R.id.clear);
		clear.setTypeface(tf);

		risultato = (ImageView) findViewById(R.id.imageView1);

		Intent intent = getIntent();
		stanza = intent.getStringExtra("numStanza");

		numStanza = stanza;

		trovato = settings.getBoolean("trovato" + numStanza, false);
		TextView tv = (TextView) findViewById(R.id.esito);
		final Button vaiCanvas = (Button) findViewById(R.id.clear);

		if (trovato) {
			vaiCanvas.setText(getResources().getString(R.string.colorPicker1));
			tv.setText(getResources().getString(R.string.Giusta));
			risultato.setImageResource(R.drawable.si);
			vaiCanvas.setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub

					new AllColorDialog(SceltaColoreOrError.this, stanza).show();

				}
			});

		} else {
			vaiCanvas.setText(getResources().getString(R.string.BackQuadro));
			tv.setText(getResources().getString(R.string.Sbagliata));
			risultato.setImageResource(R.drawable.no);

			int pos = settings.getInt("posizione", 0);
			editor = settings.edit();
			editor.putInt("coloreScelto" + pos, -1);

			editor.putString("DaRipetere" + pos, stanza);
			editor.putBoolean("MostraTutorialRipeti",true);
			editor.putBoolean("FinitoStanza" + stanza, false);
			editor.commit();
			vaiCanvas.setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					// TODO Auto-generated method stub

					Intent i = new Intent(SceltaColoreOrError.this,
							CanvasActivity.class);
					startActivity(i);
					SceltaColoreOrError.this.finish();

				}
			});
		}
	}

	@Override
	public void onBackPressed() {

		Intent i = new Intent(this, CanvasActivity.class);
		this.startActivity(i);
		SceltaColoreOrError.this.finish();
	}

}
