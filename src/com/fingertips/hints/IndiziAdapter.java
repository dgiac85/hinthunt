package com.fingertips.hints;

import java.util.List;

import com.fingertips.hinthunt.R;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class IndiziAdapter extends ArrayAdapter<String> {

	private Context context;
	int size;

	public IndiziAdapter(Context context, int ResourceId, List<String> indizi) {
		super(context, ResourceId, indizi);
		this.context = context;
		size = indizi.size();
	}

	public int getCount() {
		// TODO Auto-generated method stub
		return size;
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		String fontPath = "fonts/Noteworthy.ttc";
		Typeface tf = Typeface.createFromAsset(context.getAssets(), fontPath);

		View vista = inflater.inflate(R.layout.indizio, parent, false);
		TextView textView = (TextView) vista.findViewById(R.id.indizio);
		String indizio = getItem(position);
		textView.setTypeface(tf);
		textView.setText(indizio);
		return vista;
	}

}
