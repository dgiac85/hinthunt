package com.fingertips.twitter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.fingertips.hinthunt.R;

import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthProvider;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Prepares a OAuthConsumer and OAuthProvider
 * 
 * OAuthConsumer is configured with the consumer key & consumer secret.
 * OAuthProvider is configured with the 3 OAuth endpoints.
 * 
 * Execute the OAuthRequestTokenTask to retrieve the request, and authorize the
 * request.
 * 
 * After the request is authorized, a callback is made here.
 * 
 */
public class PrepareRequestTokenActivity extends Activity {

	final String TAG = getClass().getName();
	private final Handler mTwitterHandler = new Handler();
	private OAuthConsumer consumer;
	private OAuthProvider provider;
	Bitmap bi;
	ByteArrayInputStream bs;
	Intent i;

	@Override
	protected void onResume() {
		super.onResume();

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_browser);
		final TextView text = (TextView) findViewById(R.id.indietro);

		String fontPath = "fonts/Noteworthy.ttc";
		Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
		text.setTypeface(tf);
		text.setVisibility(View.INVISIBLE);

		byte[] abi = getIntent().getExtras().getByteArray("Bitmap");
		bi = BitmapFactory.decodeByteArray(abi, 0, abi.length);
		new Thread() {

			public void run() {

				try {

					sleep(1750);
					text.setVisibility(View.VISIBLE);

				} catch (Exception e) {

					Log.e("tag", e.getMessage());

				}

				// dismiss the progress dialog

			}

		}.start();

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		bi.compress(CompressFormat.JPEG, 100, bos);
		byte[] bitmapdata = bos.toByteArray();
		bs = new ByteArrayInputStream(bitmapdata);

		try {
			this.consumer = new CommonsHttpOAuthConsumer(
					Constants.CONSUMER_KEY, Constants.CONSUMER_SECRET);
			this.provider = new CommonsHttpOAuthProvider(Constants.REQUEST_URL,
					Constants.ACCESS_URL, Constants.AUTHORIZE_URL);
		} catch (Exception e) {
			Log.e(TAG, "Error creating consumer / provider", e);

		}

		Log.i(TAG, "Starting task to retrieve request token.");
		new OAuthRequestTokenTask(this, consumer, provider).execute();

	}

	/**
	 * Called when the OAuthRequestTokenTask finishes (user has authorized the
	 * request token). The callback URL will be intercepted here.
	 */
	@Override
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);

		final Uri uri = intent.getData();
		if (uri != null
				&& uri.getScheme().equals(Constants.OAUTH_CALLBACK_SCHEME)) {
			Log.i(TAG, "Callback received : " + uri);
			Log.i(TAG, "Retrieving Access Token");

			new RetrieveAccessTokenTask(this, consumer, provider, prefs)
					.execute(uri);
			finish();
		}
	}

	// gestione del tasto indietro
	@Override
	public void onBackPressed() {
		PrepareRequestTokenActivity.this.finish();
	}

	public class RetrieveAccessTokenTask extends AsyncTask<Uri, Void, Void> {

		private Context context;
		private OAuthProvider provider;
		private OAuthConsumer consumer;
		private SharedPreferences prefs;

		public RetrieveAccessTokenTask(Context context, OAuthConsumer consumer,
				OAuthProvider provider, SharedPreferences prefs) {
			this.context = context;
			this.consumer = consumer;
			this.provider = provider;
			this.prefs = prefs;
		}

		/**
		 * Retrieve the oauth_verifier, and store the oauth and
		 * oauth_token_secret for future API calls.
		 */
		@Override
		protected Void doInBackground(Uri... params) {
			final Uri uri = params[0];
			final String oauth_verifier = uri
					.getQueryParameter(OAuth.OAUTH_VERIFIER);

			try {
				provider.retrieveAccessToken(consumer, oauth_verifier);

				Editor edit = prefs.edit();
				edit.putString(OAuth.OAUTH_TOKEN, consumer.getToken());
				edit.putString(OAuth.OAUTH_TOKEN_SECRET,
						consumer.getTokenSecret());

				edit.commit();

				String token = prefs.getString(OAuth.OAUTH_TOKEN, "");
				String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET, "");

				consumer.setTokenWithSecret(token, secret);

				executeAfterAccessTokenRetrieval();

				Log.i(TAG, "OAuth - Access Token Retrieved");

			} catch (Exception e) {
				Log.e(TAG, "OAuth - Access Token Retrieval Error", e);
				mTwitterHandler.post(mUpdateTwitterNONotification);
			}

			return null;
		}

		final Runnable mUpdateTwitterNONotification = new Runnable() {
			public void run() {
				Toast.makeText(getBaseContext(),
						getResources().getString(R.string.twitterSN),
						Toast.LENGTH_LONG).show();
			}
		};

		final Runnable mUpdateTwitterNotification = new Runnable() {
			public void run() {
				Toast.makeText(getBaseContext(),
						getResources().getString(R.string.twitterS),
						Toast.LENGTH_SHORT).show();
				Toast.makeText(getBaseContext(),
						getResources().getString(R.string.twitterSY),
						Toast.LENGTH_LONG).show();
			}
		};

		final Runnable mUpdateTwitterSending = new Runnable() {
			public void run() {
				Toast.makeText(getBaseContext(),
						getResources().getString(R.string.twitterS),
						Toast.LENGTH_SHORT).show();

			}
		};

		private void executeAfterAccessTokenRetrieval() throws IOException {
			String msg = getIntent().getExtras().getString("tweet_msg");

			try {
				mTwitterHandler.post(mUpdateTwitterSending);
				TwitterUtils.sendTweet(prefs, msg, bs);
				mTwitterHandler.post(mUpdateTwitterNotification);
				PrepareRequestTokenActivity.this.finish();

			} catch (Exception e) {
				mTwitterHandler.post(mUpdateTwitterNONotification);
				Log.e(TAG, "OAuth - Error sending to Twitter", e);

				PrepareRequestTokenActivity.this.finish();

			}
		}
	}

}
