package com.fingertips.twitter;

import java.io.InputStream;

import oauth.signpost.OAuth;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

import android.content.SharedPreferences;
import android.util.Log;


public class TwitterUtils {

	public static boolean isAuthenticated(SharedPreferences prefs) {

		String token = prefs.getString(OAuth.OAUTH_TOKEN, "");
		String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET, "");
		try {
			AccessToken a = new AccessToken(token, secret);
			Twitter twitter = new TwitterFactory().getInstance();
			twitter.setOAuthConsumer(Constants.CONSUMER_KEY,
					Constants.CONSUMER_SECRET);
			twitter.setOAuthAccessToken(a);
			twitter.verifyCredentials();
			return true;

		} catch (TwitterException e) {
			return false;
		}
	}

	public static void sendTweet(SharedPreferences prefs, String msg,
			InputStream body) throws Exception {
		String token = prefs.getString(OAuth.OAUTH_TOKEN, "");
		String secret = prefs.getString(OAuth.OAUTH_TOKEN_SECRET, "");
		try {
			AccessToken a = new AccessToken(token, secret);
			Twitter twitter = new TwitterFactory().getInstance();
			twitter.setOAuthConsumer(Constants.CONSUMER_KEY,
					Constants.CONSUMER_SECRET);
			twitter.setOAuthAccessToken(a);

			StatusUpdate status = new StatusUpdate(msg);

			status.setMedia("name", body);
			twitter.updateStatus(status);
		} catch (TwitterException e) {
			Log.d("TAG", "Pic Upload error" + e.getErrorMessage());
			throw e;
		}
	}
}
