package com.fingertips.canvas;

import java.util.ArrayList;

import android.graphics.Path;

public class MyPath extends Path {

	ArrayList<PathCommand> l = new ArrayList<PathCommand>();
	public int color;
	public float width;

	public MyPath() {
		super();

	}

	public MyPath(MyPath mP) {
		super(mP);
		this.l = mP.l;
		this.color = mP.color;
		this.width = mP.width;
	}

	public void myReset() {
		l.clear();
		super.reset();
	}

	public void myMoveTo(float x, float y) {
		l.add(new MoveToCmd(x, y));

		super.moveTo(x, y);
	}

	public void myLineTo(float x, float y) {
		l.add(new LineToCmd(x, y));

		super.lineTo(x, y);
	}

	public void myQuadTo(float x1, float y1, float x2, float y2) {
		l.add(new QuadToCmd(x1, y1, x2, y2));

		super.quadTo(x1, y1, x2, y2);
	}

	public void setStyle(int color, float width) {
		this.color = color;
		this.width = width;

	}

	public String save() {
		String s = color + "," + width + ";";

		for (int i = 0; i < l.size(); i++) {
			s = s.concat(l.get(i).saveC());

		}
		return s;
	}
}
