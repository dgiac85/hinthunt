package com.fingertips.canvas;

public class MoveToCmd extends PathCommand {

	private float x, y;

	public MoveToCmd(float x, float y) {
		this.x = x;
		this.y = y;

	}

	public String saveC() {
		return "m" + "," + x + "," + y + ";";
	}

}
