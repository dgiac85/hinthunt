package com.fingertips.canvas;

import java.io.BufferedReader;
import com.fingertips.hinthunt.R;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import com.facebook.*;
import com.facebook.Request;
import com.facebook.Session;
import com.facebook.SessionState;
import com.fingertips.hinthunt.HintHunt;
import com.fingertips.twitter.*;
import com.fingertips.utils.ConnectionDetector;
import com.fingertips.utils.ReadJSON;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore.Images;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.View.OnClickListener;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.graphics.*;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Paint.Align;
import android.graphics.drawable.AnimationDrawable;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SlidingDrawer;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.SlidingDrawer.OnDrawerCloseListener;
import android.widget.SlidingDrawer.OnDrawerOpenListener;

import oauth.signpost.OAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CanvasActivity extends GraphicsActivity implements
		ColorPickerDialog.OnColorChangedListener,
		StrokeWidthDialog.OnWidthChangedListener {

	Button ctrlZ;
	Button ctrlY;
	Button publish, interruttore, interruttore_p, erase, hide, view;
	ImageView imageTutorial;
	TableRow tableRow;
	boolean test3;

	Button pad1, pad2, pad5;
	AnimationDrawable tutorial;
	private ProgressDialog progressDialog;
	private SharedPreferences tweetPreference;

	private boolean isDrawing = false;
	private boolean DrawMode = true;

	private final Handler mTwitterHandler = new Handler();

	Button ListHandle;
	SlidingDrawer ListDrawer;
	boolean condizioneFb;

	List<String> Permissions = Arrays.asList("publish_actions",
			"publish_stream");
	List<Uri> Uris = new ArrayList<Uri>();
	int numOpera;
	String immagine = null;
	JSONArray arrayJSON = null;
	ReadJSON obj = new ReadJSON();

	// flag for Internet connection status
	Boolean isInternetPresent = false;
	// Connection detector class
	ConnectionDetector cd;

	Disegno d;
	public int w;
	public int h;
	String orientamento;
	private Paint mPaint;
	public int mColors = 7;
	private final static String MY_PREFERENCES = "preferences";

	boolean prova1 = false;
	String text;

	String username;
	ProgressBar progressBar;

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	final Runnable mUpdateTwitterNotification = new Runnable() {
		public void run() {
			Toast.makeText(getBaseContext(), R.string.twitterSY,
					Toast.LENGTH_LONG).show();
		}
	};

	final Runnable mUpdateTwitterNONotification = new Runnable() {
		public void run() {
			Toast.makeText(getBaseContext(), R.string.twitterSN,
					Toast.LENGTH_LONG).show();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		final SharedPreferences prefs = getSharedPreferences(MY_PREFERENCES,
				Context.MODE_PRIVATE);
		boolean test3 = prefs.getBoolean("giocoFinito", false);
		boolean test = prefs.getBoolean("scelto", false);

		int pos = prefs.getInt("numeroquadro", 0);
		ReadJSON obj = new ReadJSON();
		String lingua = Locale.getDefault().getDisplayLanguage();
		try {
			if (lingua.equalsIgnoreCase("Italiano")) {
				arrayJSON = obj.leggiJSON(this, "quadriPerScelta.json");
			}
			if (lingua.equalsIgnoreCase("English")) {
				arrayJSON = obj.leggiJSON(this, "quadriPerScelta_en.json");
			}

			for (int i = 0; i < arrayJSON.length(); i++) {
				JSONObject objectJSON = arrayJSON.getJSONObject(i);
				if (i == pos)
					orientamento = objectJSON.getString("orientamento");

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (orientamento.equalsIgnoreCase("v")) {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		}

		setContentView(R.layout.activity_canvas);

		// ANIMAZIONE TUTORIAL
		imageTutorial = (ImageView) findViewById(R.id.image_tutorial);

		tutorial = (AnimationDrawable) imageTutorial.getBackground();
		tableRow = (TableRow) findViewById(R.id.tableRow1);

		this.tweetPreference = PreferenceManager
				.getDefaultSharedPreferences(this);

		// FACEBOOK
		cd = new ConnectionDetector(getApplicationContext());
		progressBar = new ProgressBar(this);
		progressBar.setVisibility(View.GONE);

		try {
			PackageManager pm = getBaseContext().getPackageManager();
			PackageInfo info = pm.getPackageInfo("com.example.hinthunt",
					PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.d("KeyHash:",
						Base64.encodeToString(md.digest(), Base64.DEFAULT));

			}
		} catch (NameNotFoundException e) {

		} catch (NoSuchAlgorithmException e) {

		}

		ListHandle = (Button) findViewById(R.id.slideButton1);
		ListDrawer = (SlidingDrawer) findViewById(R.id.SlidingDrawer1);

		ctrlZ = (Button) findViewById(R.id.ctrlz);
		ctrlY = (Button) findViewById(R.id.ctrly);
		erase = (Button) findViewById(R.id.erase);
		hide = (Button) findViewById(R.id.hide);
		view = (Button) findViewById(R.id.view_b);
		interruttore = (Button) findViewById(R.id.interruttore);
		interruttore_p = (Button) findViewById(R.id.interruttore_p);

		publish = (Button) findViewById(R.id.publish);
		pad1 = (Button) findViewById(R.id.pad1);
		pad2 = (Button) findViewById(R.id.pad2);
		pad5 = (Button) findViewById(R.id.pad5);

		view.setVisibility(View.GONE);
		interruttore_p.setVisibility(View.GONE);

		ListDrawer.setOnDrawerOpenListener(new OnDrawerOpenListener() {
			Configuration config = getResources().getConfiguration();

			public void onDrawerOpened() {

				if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
					ListHandle.setBackgroundResource(R.drawable.down_c);
				} else {
					ListHandle.setBackgroundResource(R.drawable.right);
				}
			}
		});
		ListDrawer.setOnDrawerCloseListener(new OnDrawerCloseListener() {
			Configuration config = getResources().getConfiguration();

			public void onDrawerClosed() {
				if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
					ListHandle.setBackgroundResource(R.drawable.up_c);
				} else {
					ListHandle.setBackgroundResource(R.drawable.left);
				}

			}
		});

		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setDither(true);
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeJoin(Paint.Join.ROUND);
		mPaint.setStrokeCap(Paint.Cap.ROUND);
		mPaint.setStrokeWidth(12);

		// lettura del JSON
		immagine = letturaImmagine();
		// oggetto Android per sapere la dimensione dello schermo in Pixel
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		h = displaymetrics.heightPixels;
		w = displaymetrics.widthPixels;

		// visualizzzione dell'area disegnabile della view (dimensioni)
		if (w < h) {
			if (orientamento.equalsIgnoreCase("v"))
				d = new Disegno(this, w, h);
			else
				d = new Disegno(this, h, w);
		} else if (w > h) {
			if (orientamento.equalsIgnoreCase("v"))
				d = new Disegno(this, h, w);
			else
				d = new Disegno(this, w, h);
		}

		if (!(((prefs.contains("DaRipetere0"))|| (prefs.contains("DaRipetere1"))
				|| (prefs.contains("DaRipetere2"))
				|| (prefs.contains("DaRipetere3"))
				|| (prefs.contains("DaRipetere4"))
				|| (prefs.contains("DaRipetere5"))
				|| (prefs.contains("DaRipetere6"))))){
			tutorial.start();
			imageTutorial.setVisibility(View.VISIBLE);
			ctrlZ.setEnabled(false);
			ctrlY.setEnabled(false);
			hide.setEnabled(false);
			erase.setEnabled(false);
			interruttore.setEnabled(false);
			interruttore_p.setEnabled(false);
			view.setEnabled(false);

			ListDrawer.setVisibility(View.INVISIBLE);
		} else {
			imageTutorial.setVisibility(View.INVISIBLE);
			ctrlZ.setEnabled(true);
			ctrlY.setEnabled(true);
			hide.setEnabled(true);
			erase.setEnabled(true);
			interruttore.setEnabled(true);
			interruttore_p.setEnabled(true);
			view.setEnabled(true);

			ListDrawer.setVisibility(View.VISIBLE);
		}

		FrameLayout preview = (FrameLayout) findViewById(R.id.paint);
		preview.addView(d);

		mPaint.setColor(lastColor());

		hide.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				// se mode scelta è hide nascondi tutti i bottoni
				erase.setVisibility(View.GONE);
				ctrlZ.setVisibility(View.GONE);
				ctrlY.setVisibility(View.GONE);
				interruttore.setVisibility(View.GONE);
				interruttore_p.setVisibility(View.GONE);
				hide.setVisibility(View.GONE);
				view.setVisibility(View.VISIBLE);

			}

		});

		view.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				// se mode scelta è view:
				erase.setVisibility(View.VISIBLE);
				ctrlZ.setVisibility(View.VISIBLE);
				ctrlY.setVisibility(View.VISIBLE);
				interruttore.setVisibility(View.VISIBLE);
				interruttore_p.setVisibility(View.VISIBLE);
				hide.setVisibility(View.VISIBLE);
				view.setVisibility(View.GONE);

			}

		});

		pad1.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				uno();

			}
		});
		pad2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				due();

			}
		});

		// TWITTER
		pad5.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				isInternetPresent = cd.isConnectingToInternet();

				if (!isInternetPresent) {
					showAlertDialog(CanvasActivity.this, "Error",
							getResources().getString(R.string.connessione),
							false, false);
					return;
				}

				boolean installed = appInstalledOrNot("com.twitter.android");
				if (installed) {
					final Dialog dialog = new Dialog(CanvasActivity.this);
					dialog.setTitle(getResources().getString(R.string.twitterC));

					dialog.setContentView(R.layout.dontaskagain);

					dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
					final CheckBox dontShowAgain = (CheckBox) dialog
							.getWindow().findViewById(R.id.skip);
					Button btnDismiss = (Button) dialog.getWindow()
							.findViewById(R.id.tweet);
					Button btnOk = (Button) dialog.getWindow().findViewById(
							R.id.clear);
					SharedPreferences settings1 = getSharedPreferences(
							MY_PREFERENCES, 0);
					String skipMessage = settings1.getString("skipMessage1",
							"NOT checked");
					if (!skipMessage.equals("checked"))
						dialog.show();
					else {
						if (settings1.getBoolean("siAppTw", false))
							apriTw(v);
						else
							mostraDialogTwitter();
					}
					btnDismiss.setOnClickListener(new OnClickListener() {

						public void onClick(View v) {

							String checkBoxResult = "NOT checked";
							if (dontShowAgain.isChecked())
								checkBoxResult = "checked";
							SharedPreferences settings = getSharedPreferences(
									MY_PREFERENCES, 0);
							SharedPreferences.Editor editor = settings.edit();
							editor.putString("skipMessage1", checkBoxResult);
							editor.putBoolean("siAppTw", false);

							editor.commit();
							mostraDialogTwitter();
							dialog.dismiss();
						}
					});

					btnOk.setOnClickListener(new OnClickListener() {
						public void onClick(View v) {
							String checkBoxResult = "NOT checked";
							if (dontShowAgain.isChecked())
								checkBoxResult = "checked";
							SharedPreferences settings = getSharedPreferences(
									MY_PREFERENCES, 0);
							SharedPreferences.Editor editor = settings.edit();
							editor.putString("skipMessage1", checkBoxResult);
							editor.putBoolean("siAppTw", true);

							editor.commit();

							dialog.dismiss();
							apriTw(v);

						}

					});

				} else {

					final Dialog dialogTw = new Dialog(CanvasActivity.this);
					dialogTw.setTitle("Twitter");

					dialogTw.setContentView(R.layout.dialogtwitter);
					dialogTw.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

					Button tweet = (Button) dialogTw.getWindow().findViewById(
							R.id.tweet);
					TextView textAuthTweet = (TextView) dialogTw.getWindow()
							.findViewById(R.id.loginStatus);
					Button clearCredentials = (Button) dialogTw.getWindow()
							.findViewById(R.id.clear);
					Button exit = (Button) dialogTw.getWindow().findViewById(
							R.id.exit);
					SharedPreferences settings = getSharedPreferences(
							MY_PREFERENCES, 0);

					if (settings.getBoolean("authTweet", false)) {
						textAuthTweet.setText(R.string.twitterY);
						clearCredentials.setEnabled(true);
					} else {
						textAuthTweet.setText(R.string.twitterN);
						clearCredentials.setEnabled(false);
					}

					final EditText testo = (EditText) dialogTw.getWindow()
							.findViewById(R.id.tweetText);

					tweet.setOnClickListener(new OnClickListener() {

						public void onClick(View v) {

							Toast.makeText(
									CanvasActivity.this,
									getResources().getString(R.string.twitterS),
									Toast.LENGTH_SHORT).show();
							text = testo.getText().toString();
							OAuthTweet();

							dialogTw.dismiss();
						}
					});

					clearCredentials.setOnClickListener(new OnClickListener() {

						public void onClick(View v) {
							clearCredentials();
							updateLoginStatus();
							Toast.makeText(
									CanvasActivity.this,
									getResources().getString(R.string.twitterR),
									Toast.LENGTH_LONG).show();
							dialogTw.dismiss();
						}
					});

					exit.setOnClickListener(new OnClickListener() {

						public void onClick(View v) {

							dialogTw.dismiss();
						}
					});

					dialogTw.show();

				}

			}
		});

		ctrlZ.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				d.CtrlZ();
			}

		});

		ctrlY.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				d.CtrlY();
			}
		});

		// INTERRUTTORE MOVE
		interruttore.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				DrawMode = false;
				interruttore.setVisibility(View.GONE);
				interruttore_p.setVisibility(View.VISIBLE);

			}

		});

		// INTERRUTTORE MOVE
		interruttore_p.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				DrawMode = true;
				interruttore_p.setVisibility(View.GONE);
				interruttore.setVisibility(View.VISIBLE);

			}

		});

		erase.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				mPaint.setColor(Color.WHITE);
			}

		});

		publish.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				isInternetPresent = cd.isConnectingToInternet();

				if (!isInternetPresent) {
					showAlertDialog(
							CanvasActivity.this,
							"Error",
							getResources().getString(R.string.connessione),
							false, false);
					return;
				} else {

				}

				if (!isFbApp() && HintHunt.firstUse) {

					HintHunt.firstUse = false;
					showAlertDialog(
							CanvasActivity.this,
							" ",
							getResources().getString(R.string.facebookA),
							false, true);

					return;
				} else {
					mostraDialogFacebook();

				}

			}

		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_canvas, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Bitmap bi = null;
		switch (item.getItemId()) {
		case R.id.help:
			// app icon in action bar clicked; go home

			tutorial.start();
			imageTutorial.setVisibility(View.VISIBLE);
			ctrlZ.setEnabled(false);
			ctrlY.setEnabled(false);
			hide.setEnabled(false);
			erase.setEnabled(false);
			interruttore.setEnabled(false);
			interruttore_p.setEnabled(false);
			view.setEnabled(false);

			ListDrawer.setVisibility(View.INVISIBLE);
			return true;
		case R.id.menu_settingsa:
			// app icon in action bar clicked; go home

			SharedPreferences prefs = getSharedPreferences(MY_PREFERENCES,
					Context.MODE_PRIVATE);
			final Editor edit = prefs.edit();

			edit.remove("skipMessage1");
			edit.remove("siAppTw");
			edit.commit();
			Toast.makeText(CanvasActivity.this,
					getResources().getString(R.string.twitterD),
					Toast.LENGTH_LONG).show();

			return true;
		case R.id.menu_settingsb:
			// app icon in action bar clicked; go home
			this.infoPunteggio();
			return true;

		case R.id.menu_settingsc:
			// app icon in action bar clicked; go home
			try {
				bi = d.Export();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			bi.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
			String path = Images.Media.insertImage(getBaseContext()
					.getContentResolver(), bi, "#HintHuntDegasEdition", null);
			Toast.makeText(CanvasActivity.this,
					getResources().getString(R.string.savePic),
					Toast.LENGTH_LONG).show();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	private void infoPunteggio() {
		// TODO Auto-generated method stub
		final Dialog dialogInfo = new Dialog(CanvasActivity.this);
      	dialogInfo.setTitle("Score");
      	
      	dialogInfo.setContentView(R.layout.infopunteggio);            	
      	dialogInfo.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
      	
 		
      	 Button exit = (Button)dialogInfo.getWindow().findViewById(R.id.exit1);
 		 TextView text=(TextView) dialogInfo.getWindow().findViewById(R.id.TextView01);
 		 TextView text1=(TextView) dialogInfo.getWindow().findViewById(R.id.textView1);
 		 TextView indizi=(TextView) dialogInfo.getWindow().findViewById(R.id.IndiziUtilizzati);
 		 TextView colori=(TextView) dialogInfo.getWindow().findViewById(R.id.ColoriOttenuti);
 		 String fontPath = "fonts/Noteworthy.ttc";
         Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
 		 text.setTypeface(tf);
 		 text1.setTypeface(tf);
 		 indizi.setTypeface(tf);
 		 colori.setTypeface(tf);
 		
 		 SharedPreferences punteggio = getSharedPreferences(MY_PREFERENCES, 0);
 		 text.setText(getResources().getString(R.string.photo2));
 		 text1.setText(getResources().getString(R.string.photo1));
 		 indizi.setText(""+punteggio.getInt("INDIZI",0));
 		 colori.setText(""+d.totColors());
      
 	
  	exit.setOnClickListener(new OnClickListener(){

  		public void onClick(View v) { 			
  			dialogInfo.dismiss();
  	}});
  	
  	dialogInfo.show();
		
	}

	public void mostraDialogTwitter() {
		final Dialog dialogTw = new Dialog(CanvasActivity.this);
		dialogTw.setTitle("Twitter");

		dialogTw.setContentView(R.layout.dialogtwitter);
		dialogTw.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

		Button tweet = (Button) dialogTw.getWindow().findViewById(R.id.tweet);
		Button exit = (Button) dialogTw.getWindow().findViewById(R.id.exit);
		TextView textAuthTweet = (TextView) dialogTw.getWindow().findViewById(
				R.id.loginStatus);
		Button clearCredentials = (Button) dialogTw.getWindow().findViewById(
				R.id.clear);
		SharedPreferences settings = getSharedPreferences(MY_PREFERENCES, 0);

		if (settings.getBoolean("authTweet", false)) {
			textAuthTweet.setText(R.string.twitterY);
			clearCredentials.setEnabled(true);
		} else {
			textAuthTweet.setText(R.string.twitterN);
			clearCredentials.setEnabled(false);
		}

		System.out.println("Cliccato!");

		final EditText testo = (EditText) dialogTw.getWindow().findViewById(
				R.id.tweetText);

		tweet.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				Toast.makeText(CanvasActivity.this,
						getResources().getString(R.string.loadingText),
						Toast.LENGTH_SHORT).show();
				text = testo.getText().toString();
				OAuthTweet();

				dialogTw.dismiss();
			}
		});

		clearCredentials.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				clearCredentials();
				updateLoginStatus();
				Toast.makeText(CanvasActivity.this,
						getResources().getString(R.string.twitterR),
						Toast.LENGTH_LONG).show();
				dialogTw.dismiss();
			}
		});

		dialogTw.show();
	}

	public void mostraDialogFacebook() {
		final Dialog dialogFb = new Dialog(CanvasActivity.this);
		dialogFb.setTitle("Facebook");

		dialogFb.setContentView(R.layout.dialogfacebook);
		dialogFb.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

		Button post = (Button) dialogFb.getWindow().findViewById(R.id.post);
		Button exit = (Button) dialogFb.getWindow().findViewById(R.id.exit);


		final EditText testo = (EditText) dialogFb.getWindow().findViewById(
				R.id.faceText);

		post.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				Toast.makeText(CanvasActivity.this,
						getResources().getString(R.string.loadingText),
						Toast.LENGTH_SHORT).show();
				text = testo.getText().toString();
				try {
					dialogFb.dismiss();
					publish(text);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		exit.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				dialogFb.dismiss();
			}
		});

		dialogFb.show();
	}

	public void apriTw(View v) {
		Bitmap bi;
		try {
			bi = d.Export();
			ByteArrayOutputStream bytes = new ByteArrayOutputStream();
			bi.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
			String path = Images.Media.insertImage(getBaseContext()
					.getContentResolver(), bi, "#HintHuntDegasEdition", null);
			Uri ss2 = Uri.parse(path);
			Uris.add(ss2);
			Toast.makeText(CanvasActivity.this, R.string.savePic,
					Toast.LENGTH_LONG).show();

			if (isThisApplicationRunning(getApplicationContext(),
					"com.twitter.android")) {

				final ActivityManager manager = (ActivityManager) getApplicationContext()
						.getSystemService(Context.ACTIVITY_SERVICE);
				final List<RunningAppProcessInfo> runningAppProcesses = manager
						.getRunningAppProcesses();
				for (final RunningAppProcessInfo app : runningAppProcesses) {
					if (app.processName == "com.twitter.android") {
						System.out.println("entra");
						android.os.Process.killProcess(app.pid);
					}
				}

			} else {

			}

			Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);

			shareIntent.setType("image/*");
			shareIntent.putExtra(android.content.Intent.EXTRA_TEXT,
					"@the_fingertips #HintHuntDegas");

			shareIntent.putExtra(Intent.EXTRA_STREAM, ss2);

			PackageManager pm = v.getContext().getPackageManager();
			List<ResolveInfo> activityList = pm.queryIntentActivities(
					shareIntent, 0);
			for (final ResolveInfo app : activityList) {
				if ("com.twitter.android.PostActivity"
						.equals(app.activityInfo.name)) {
					final ActivityInfo activity = app.activityInfo;
					final ComponentName name = new ComponentName(
							activity.applicationInfo.packageName, activity.name);
					shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);

					shareIntent.setComponent(name);
					v.getContext().startActivity(shareIntent);

					break;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void OAuthTweet() {

		/**
		 * Send a tweet. If the user hasn't authenticated to Tweeter yet, he'll
		 * be redirected via a browser to the twitter login page. Once the user
		 * authenticated, he'll authorize the Android application to send tweets
		 * on the users behalf.
		 */

		new AsyncTask<SharedPreferences, Object, Boolean>() {

			protected Boolean doInBackground(SharedPreferences... params) {
				return TwitterUtils.isAuthenticated(params[0]);
			}

			protected void onPostExecute(Boolean isAuthenticated) {
				if (isAuthenticated) {
					// Do processing after successful authentication
					// Toast.makeText(CanvasActivity.this,"Invio in corso...Fra poco saprai se l'operzione è andata in porto!",
					// Toast.LENGTH_LONG).show();

					sendTweet();
					progressDialog = ProgressDialog.show(CanvasActivity.this,
							getResources().getString(R.string.loadingText),getResources().getString(R.string.title_activity_loading));

					new Thread() {

						public void run() {

							try {

								sleep(1750);
							} catch (Exception e) {

								Log.e("tag", e.getMessage());

							}

							// dismiss the progress dialog

							progressDialog.dismiss();

						}

					}.start();
				} else {
					Toast.makeText(
							CanvasActivity.this,
							"Tra poco verrai redirezionato al browser per l'autenticazione. Successivamente verrai riportato ad HintHunt",
							Toast.LENGTH_LONG).show();
					// Do processing after authentication failure
					Bitmap bi = null;

					try {
						bi = d.Export();
						ByteArrayOutputStream bos = new ByteArrayOutputStream();
						bi.compress(CompressFormat.PNG, 0, bos);
						byte[] bitmapdata = bos.toByteArray();
						Intent i = new Intent(getApplicationContext(),
								PrepareRequestTokenActivity.class);
						i.putExtra("tweet_msg", getTweetMsg());

						i.putExtra("Bitmap", bitmapdata);
						startActivity(i);
						// CanvasActivity.this.finish();

					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

				}
			}
		}.execute(tweetPreference);
		// }
		// });
	}

	@Override
	public void onBackPressed() {
		this.finish();
	}

	private void updateLoginStatus() {

		new AsyncTask<SharedPreferences, Object, Boolean>() {

			protected Boolean doInBackground(SharedPreferences... params) {
				return TwitterUtils.isAuthenticated(params[0]);
			}

			protected void onPostExecute(Boolean isAuthenticated) {
				SharedPreferences settings = getSharedPreferences(
						MY_PREFERENCES, 0);
				SharedPreferences.Editor editor = settings.edit();
				if (isAuthenticated) {
					editor.putBoolean("authTweet", true);
					editor.commit();
					// loginStatus.setText("Logged into Twitter!"+" Yes!");
				} else {
					editor.putBoolean("authTweet", false);
					editor.commit();
					// loginStatus.setText("Logged into Twitter!"+" No!");
				}
			}
		}.execute(tweetPreference);

	}

	private String getTweetMsg() {
		// EditText text=(EditText)findViewById(R.id.tweetText);
		return "@the_fingertips #HintHuntDegas" + " " + text;
	}

	public void sendTweet() {
		Bitmap bi = null;
		try {
			bi = d.Export();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		bi.compress(CompressFormat.JPEG, 100, bos);
		byte[] bitmapdata = bos.toByteArray();
		final ByteArrayInputStream bs = new ByteArrayInputStream(bitmapdata);
		Toast.makeText(getBaseContext(), "Invio in Corso!", Toast.LENGTH_LONG)
				.show();
		Thread t = new Thread() {
			public void run() {

				try {
					/*
					 * String message="ciao"; File file=new File("ciao");
					 * StatusUpdate status = new StatusUpdate(message);
					 * status.setMedia(file); //mTwitter.updateStatus(status);
					 */

					// InputStream imgFile = getAssets().open("icon.png");//File
					// imgFile = new File("assets/icon.png");

					TwitterUtils.sendTweet(tweetPreference, getTweetMsg(), bs);
					mTwitterHandler.post(mUpdateTwitterNotification);
				} catch (Exception ex) {
					mTwitterHandler.post(mUpdateTwitterNONotification);
					ex.printStackTrace();
				}
			}

		};
		t.start();
	}

	private void clearCredentials() {
		SharedPreferences prefs = PreferenceManager
				.getDefaultSharedPreferences(this);
		final Editor edit = prefs.edit();
		edit.remove(OAuth.OAUTH_TOKEN);
		edit.remove(OAuth.OAUTH_TOKEN_SECRET);
		edit.commit();
	}

	public void publish(final String testo) throws IOException {

		final Bitmap bi = d.Export();
		Toast.makeText(CanvasActivity.this, R.string.facebookS,
				Toast.LENGTH_LONG).show();

		Session.openActiveSession(CanvasActivity.this, true,
				new Session.StatusCallback() {

					public void call(Session session, SessionState state,
							Exception exception) {

						String testo1 = testo;
						if (session.isOpened()) {

							if (state == state.OPENED && isFbApp()) {

								Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(
										CanvasActivity.this, Permissions);
								Session.getActiveSession()
										.requestNewPublishPermissions(
												newPermissionsRequest);

							}

							if (state == state.OPENED_TOKEN_UPDATED) {
								Request photoRequest = Request
										.newUploadPhotoRequest(session, bi,
												new Request.Callback() {

													public void onCompleted(
															Response response) {
														// messaggi di errore

														if (response.getError() != null) {
															System.out
																	.println(response
																			.getError()
																			.getErrorMessage());
															Toast.makeText(
																	CanvasActivity.this,
																	R.string.facebookSN,
																	Toast.LENGTH_SHORT)
																	.show();
														}

														else {
															System.out
																	.println("Pubblicata");
															Toast.makeText(
																	CanvasActivity.this,
																	R.string.facebookSY,
																	Toast.LENGTH_SHORT)
																	.show();
														}

													}
												});

								Bundle params = photoRequest.getParameters();

								// Add the parameters you want, the caption in
								// this case

								params.putString("message", testo1);

								// Update the request parameters
								photoRequest.setParameters(params);
								photoRequest.executeAsync();

							}

						}

					}

				});
	}

	@SuppressWarnings("deprecation")
	public void showAlertDialog(Context context, String title, String message,
			Boolean status, final Boolean publish) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		// Setting Dialog Title
		alertDialog.setTitle(title);

		// Setting Dialog Message
		alertDialog.setMessage(message);

		// Setting alert dialog icon
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if (publish) {
					try {
						publish(text);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				dialog.dismiss();
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}

	public boolean isFbApp() {

		try {
			ApplicationInfo info = getPackageManager().getApplicationInfo(
					"com.facebook.katana", 0);
			return true;
		} catch (PackageManager.NameNotFoundException e) {
			return false;
		}

	}

	// settare colore
	public void colorChanged(int color) {
		mPaint.setColor(color);
	}

	// settare spessore tratto
	public void WidthChanged(int valore) {
		mPaint.setStrokeWidth(valore);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Session.getActiveSession().onActivityResult(this, requestCode,
				resultCode, data);
	}

	// view - colorare -
	public class Disegno extends View {
		private static final float MINP = 0.25f;
		private static final float MAXP = 0.75f;

		// tratti rasterizzati sull'immagine
		private Bitmap mBitmap;
		// immagine in bianco e nero adattata
		private Bitmap mSfondo;
		private InputStream sfondoStream = null;
		private Canvas mCanvas;
		// path temporaneo non ancora salvato --> buffer di disegno
		private MyPath mPath;

		private Paint temPaint;
		private Paint mBitmapPaint;
		private float x, y, size;
		// scalo e translo
		private Matrix m;
		// matrice per pubblicare (matrice identità)
		private Matrix m2 = new Matrix();
		private int dw, dh;
		// oggetto che vede quando uso due dita
		private ScaleGestureDetector sgd;
		private long lastPinch = 0;

		private ArrayList<MyPath> doPaths = new ArrayList<MyPath>();
		private ArrayList<MyPath> undoPaths = new ArrayList<MyPath>();

		public Disegno(Context c, int w, int h) {
			super(c);

			dw = w;
			dh = h;

			mPath = new MyPath();

			mBitmapPaint = new Paint(Paint.DITHER_FLAG);

			temPaint = new Paint(mPaint);

			mBitmap = Bitmap.createBitmap(dw, dh, Bitmap.Config.ARGB_8888);

			mCanvas = new Canvas(mBitmap);

			if (HintHunt.saveSfondo != null) {
				mSfondo = HintHunt.saveSfondo;
			}

			else {

				try {
					sfondoStream = this.getResources().getAssets()
							.open("immaginiBN/" + immagine);

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				mSfondo = BitmapFactory.decodeStream(sfondoStream);

				int[] pixels = new int[mSfondo.getWidth() * mSfondo.getHeight()];

				mSfondo.getPixels(pixels, 0, mSfondo.getWidth(), 0, 0,
						mSfondo.getWidth(), mSfondo.getHeight());
				// int[] pixels2 = new int[pixels.length];

				for (int i = 0; i < pixels.length; i++) {

					// Casomai l'immagine serigrafata fosse a 24 bit invece che
					// 8:
					int p = pixels[i];
					int r = (p & 0xff0000) >> 16;
					int g = (p & 0xff00) >> 8;
					int b = p & 0xff;
					int l = luminance(r, g, b);
					l = 255 - l;

					pixels[i] = Color.argb(l, 0, 0, 0);

				}

				mSfondo = Bitmap.createBitmap(pixels, mSfondo.getWidth(),
						mSfondo.getHeight(), Bitmap.Config.ARGB_8888);
				float aspect = (float) mSfondo.getWidth()
						/ (float) mSfondo.getHeight();
				float screenAspect = dw / dh; // questo � per i cellulari
												// landscape, da testare

				mSfondo = Bitmap.createScaledBitmap(mSfondo, dw,
						(int) (dw / aspect), true);

				HintHunt.saveSfondo = mSfondo;

			}

			x = 0;
			y = 0;
			size = 1f;
			m = new Matrix();
			m.setScale(size, size);

			sgd = new ScaleGestureDetector(getContext(),
					new ScaleGestureDetector.OnScaleGestureListener() {

						PointF center = new PointF();

						public void onScaleEnd(ScaleGestureDetector detector) {

							if (isDrawing) {
								touch_up(true);
							}
							invalidate();
							lastPinch = System.currentTimeMillis();

						}

						public boolean onScaleBegin(
								ScaleGestureDetector detector) {

							center.x = detector.getFocusX();
							center.y = detector.getFocusY();
							invalidate();
							lastPinch = System.currentTimeMillis();

							return true;
						}

						public boolean onScale(ScaleGestureDetector detector) {
							float cx = detector.getFocusX();
							float cy = detector.getFocusY();
							float f = detector.getScaleFactor();
							float ox = (cx / size + x);
							float oy = (cy / size + y);
							// size*=f;

							size = Math.min(3, f * size);
							size = Math.max(1f, size);

							x = ox - cx / size - (cx - center.x) / size;
							y = oy - cy / size - (cy - center.y) / size;

							float limit = 0.66f;
							// limite di movimento dell'immagine
							if (x <= -dw * limit / size)
								x = -dw * limit / size + 1;
							else if (x >= mSfondo.getWidth() * limit)
								x = mSfondo.getWidth() * limit;

							if (y <= -dh * limit / size)
								y = -dh * limit / size + 1;
							else if (y >= mSfondo.getHeight() * limit)
								y = mSfondo.getHeight() * limit;

							center.x = cx;
							center.y = cy;

							m.setScale(size, size);
							m.postTranslate(-x * size, -y * size);

							float pts[] = new float[] { ox, oy };
							m.mapPoints(pts);

							invalidate();
							lastPinch = System.currentTimeMillis();

							return true;
						}
					});
		}

		// tasto or spostare/disegnare
		public void setDrawMode(boolean set) {

			DrawMode = set;
		}

		private int luminance(float r, float g, float b) {
			return Math.round(0.299f * r + 0.587f * g + 0.114f * b);
		}

		@Override
		protected void onDraw(Canvas canvas) {
			canvas.drawColor(Color.WHITE);

			// disegno i tratti serializzati
			canvas.drawBitmap(mBitmap, m, mBitmapPaint);

			canvas.save();
			canvas.concat(m);

			// disegna i tratti attuali real time
			canvas.drawPath(mPath, mPaint);
			canvas.restore();
			// ridisegna lo sfondo b/n
			canvas.drawBitmap(mSfondo, m, mBitmapPaint);

		}

		private float mX, mY;

		// tolleranza al touch
		private static final float TOUCH_TOLERANCE = 4;

		private void touch_start(float tx, float ty) {

			if (DrawMode) {
				mPath.myReset();
				// muovere dove sto cliccando con le trasformazioni della
				// matrice
				mPath.myMoveTo(tx / size + x, ty / size + y);
			}

			mX = tx;
			mY = ty;

		}

		private void touch_move(float tx, float ty) {
			float dx = Math.abs(tx - mX);
			float dy = Math.abs(ty - mY);
			if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
				if (DrawMode) {
					// disegno
					mPath.myQuadTo(mX / size + x, mY / size + y, (tx / size + x
							+ mX / size + x) / 2,
							(ty / size + y + mY / size + y) / 2);
					isDrawing = true;
				} else {
					// sposto
					x -= (tx - mX) / size;
					y -= (ty - mY) / size;

					float limit = 0.66f;

					if (x <= -w * limit / size)
						x = -w * limit / size + 1;
					else if (x >= mSfondo.getWidth() * limit)
						x = mSfondo.getWidth() * limit;

					if (y <= -h * limit / size)
						y = -h * limit / size + 1;
					else if (y >= mSfondo.getHeight() * limit)
						y = mSfondo.getHeight() * limit;

					m.setScale(size, size);
					m.postTranslate(-x * size, -y * size);

				}

				mX = tx;
				mY = ty;
			}

		}

		private void touch_up(boolean scale) {

			if (DrawMode) {

				if (!scale)
					mPath.myLineTo(mX / size + x, mY / size + y);

				mCanvas.drawPath(mPath, mPaint);
				mPath.setStyle(mPaint.getColor(), mPaint.getStrokeWidth());
				doPaths.add(mPath);
				mPath = new MyPath();
			}

			isDrawing = false;

		}

		@Override
		public boolean onTouchEvent(MotionEvent event) {

			// if (!isDrawing)
			sgd.onTouchEvent(event); // aumentare 100 se serve

			if (event.getPointerCount() == 1
					&& System.currentTimeMillis() - lastPinch > 150) {
				// float ex = event.getX()/size+x;
				// float ey = event.getY()/size+y;

				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:

					tutorial.stop();
					imageTutorial.setVisibility(GONE);

					ctrlZ.setEnabled(true);
					ctrlY.setEnabled(true);
					hide.setEnabled(true);
					erase.setEnabled(true);
					interruttore.setEnabled(true);
					interruttore_p.setEnabled(true);
					view.setEnabled(true);

					ListDrawer.setVisibility(View.VISIBLE);
					touch_start(event.getX(), event.getY());

					invalidate();
					break;

				case MotionEvent.ACTION_MOVE:

					touch_move(event.getX(), event.getY());

					invalidate();
					break;

				case MotionEvent.ACTION_UP:

					touch_up(false);
					invalidate();
					break;
				}
			}
			return true;
		}

		public void CtrlZ() {

			if (!doPaths.isEmpty()) {

				undoPaths.add(doPaths.get(doPaths.size() - 1));
				doPaths.remove(doPaths.size() - 1);

				Refresh();
			}

		}

		public void CtrlY() {

			if (!undoPaths.isEmpty()) {
				doPaths.add(undoPaths.get(undoPaths.size() - 1));

				undoPaths.remove(undoPaths.size() - 1);

				Refresh();
			}
		}

		// scrive un file di testo con i doPath
		public void Serialize() throws IOException {

			String serial;
			FileOutputStream outputStream;
			outputStream = openFileOutput("Paths.txt", Context.MODE_PRIVATE);

			for (int i = 0; i < doPaths.size(); i++) {
				serial = doPaths.get(i).save() + "\n";
				outputStream.write(serial.getBytes());
				// System.out.println("Path"+i+" "+serial);
			}
			outputStream.close();
			doPaths.clear();
		}

		// serve per leggere il file di testo e capire dove sono i path
		// precedentemente disegnati e salvarli in doPath
		public void Pathize() throws IOException {

			MyPath nPath = new MyPath();
			String StringPath;
			FileInputStream open = openFileInput("Paths.txt");
			InputStreamReader isr = new InputStreamReader(open);
			BufferedReader buff = new BufferedReader(isr);

			StringPath = buff.readLine();

			while (StringPath != null) {

				nPath = new MyPath();

				String[] Sections = StringPath.split(";");

				String[] Style = Sections[0].split(",");

				nPath.setStyle(Integer.parseInt(Style[0]),
						Float.parseFloat(Style[1]));

				for (int i = 1; i < Sections.length; i++) {

					String[] Command = Sections[i].split(",");

					float x1, y1, x2, y2;

					if (Command[0].equalsIgnoreCase("m")) {

						x1 = Float.parseFloat(Command[1]);
						y1 = Float.parseFloat(Command[2]);
						nPath.myMoveTo(x1, y1);

					}

					if (Command[0].equalsIgnoreCase("q")) {

						x1 = Float.parseFloat(Command[1]);
						y1 = Float.parseFloat(Command[2]);
						x2 = Float.parseFloat(Command[3]);
						y2 = Float.parseFloat(Command[4]);
						nPath.myQuadTo(x1, y1, x2, y2);
					}

					if (Command[0].equalsIgnoreCase("l")) {

						x1 = Float.parseFloat(Command[1]);
						y1 = Float.parseFloat(Command[2]);
						nPath.myLineTo(x1, y1);
					}

				}

				d.doPaths.add(nPath);

				StringPath = buff.readLine();
			}

			Refresh();

		}

		public void Refresh() {

			mCanvas.drawColor(Color.WHITE);

			for (int i = 0; i < doPaths.size(); i++) {

				temPaint.setColor(doPaths.get(i).color);
				temPaint.setStrokeWidth(doPaths.get(i).width);

				mCanvas.drawPath(doPaths.get(i), temPaint);
			}

			invalidate();
		}

		public int totColors() {
			final SharedPreferences prefs = getSharedPreferences(
					MY_PREFERENCES, Context.MODE_PRIVATE);
			int totColors = 0;

			for (int i = 0; i <= mColors; i++) {
				if (prefs.contains("coloreScelto" + i)) {

					if (prefs.getInt("coloreScelto" + i, 1) != -1)
						totColors++;
				}
			}
			return totColors;

		}

		public Bitmap Export() throws IOException {

			Bitmap overlay = mSfondo.copy(mSfondo.getConfig(), true);

			Refresh();

			int adjust = (w + h) / 2;

			String fontPath = "fonts/Noteworthy.ttc";
			Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);

			SharedPreferences punteggio = getSharedPreferences(MY_PREFERENCES,
					0);

			String colors = getResources().getString(R.string.photo1)
					+ totColors() + " ";
			String hints = getResources().getString(R.string.photo2)
					+": "+punteggio.getInt("INDIZI", 0);

			mCanvas.drawBitmap(overlay, m2, mBitmapPaint);
			temPaint.setTypeface(tf);
			temPaint.setColor(Color.WHITE);
			temPaint.setStrokeWidth(adjust / 80);
			temPaint.setTextAlign(Align.RIGHT);
			temPaint.setTextSize(adjust / 32);
			mCanvas.drawText(colors + hints, w - 10, h - 20, temPaint);
			temPaint.setColor(Color.BLACK);
			temPaint.setStrokeWidth(adjust / 320);
			mCanvas.drawText(colors + hints, w - 10, h - 20, temPaint);

			return mBitmap.copy(mBitmap.getConfig(), true);
		}

	}

	// tavolozza colori
	public void uno() {

		mPaint.setXfermode(null);
		mPaint.setAlpha(0xFF);
		new ColorPickerDialog(this, this, mPaint.getColor()).show();
		prova1 = true;
	}

	// spessore del pennello
	public void due() {
		new StrokeWidthDialog(this, this, mPaint.getStrokeWidth()).show();
	}

	static int pid;

	public static boolean isThisApplicationRunning(final Context context,
			final String appPackage) {
		if (appPackage == null) {
			return false;
		}
		final ActivityManager manager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		final List<RunningAppProcessInfo> runningAppProcesses = manager
				.getRunningAppProcesses();
		for (final RunningAppProcessInfo app : runningAppProcesses) {
			if (appPackage.equals(app.processName)) {
				pid = app.pid;
				return true;
			}
		}
		return false;
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		Log.i(this.getClass().toString(), "Richiamato onRestart()");
	}

	@Override
	protected void onStart() {
		super.onStart();
		SharedPreferences prefs = getSharedPreferences(MY_PREFERENCES,
				Context.MODE_PRIVATE);
		boolean test = prefs.getBoolean("scelto", false);
		/*
		 * if (orientamento.equalsIgnoreCase("v")){
		 * setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); }
		 * else{
		 * setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE); }
		 */
		Log.i(this.getClass().toString(), "Richiamato onStart()");
	}

	@Override
	protected void onResume() {
		super.onResume();

		mPaint.setColor(lastColor());// prendi the last color
		// ListDrawer.setVisibility(View.VISIBLE);
		ListDrawer.open();
		final SharedPreferences prefs = getSharedPreferences(MY_PREFERENCES,
				Context.MODE_PRIVATE);
		/*if(prefs.getBoolean("MostraTutorialRipeti", false)){
			mPaint.setXfermode(null);
			mPaint.setAlpha(0xFF);
			new ColorPickerDialog(this, this, mPaint.getColor()).show();
		}*/
			
		test3 = prefs.getBoolean("giocoFinito", false);
		if (test3) {
			// mostra dialog finito gioco con non mostrare più
		}
		updateLoginStatus();

		try {
			d.Pathize();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		immagine = letturaImmagine();
		prova1 = false;
		Log.i(this.getClass().toString(), "Richiamato onResume()");

	}

	@Override
	protected void onPause() {
		super.onPause();

		// HintHunt.saveBitmap=d.mBitmap.copy(d.mBitmap.getConfig(), true);
		try {
			d.Serialize();
			// CanvasActivity.this.finish();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		Log.i(this.getClass().toString(), "Richiamato onPause()");
	}

	@Override
	protected void onStop() {
		super.onStop();
		SharedPreferences prefs = getSharedPreferences(MY_PREFERENCES,
				Context.MODE_PRIVATE);
		boolean test = prefs.getBoolean("scelto", false);
		int pos = prefs.getInt("pos", 0);
		Log.i(this.getClass().toString(), "Richiamato onStop()");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		Log.i(this.getClass().toString(), "Richiamato onDestroy()");
	}

	private String letturaImmagine() {

		String img = null;
		SharedPreferences prefs = getSharedPreferences(MY_PREFERENCES,
				Context.MODE_PRIVATE);
		numOpera = prefs.getInt("numeroquadro", -1);
		try {
			arrayJSON = obj.leggiJSON(this, "quadriBNForCanvasActivity.json");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			for (int i = 0; i < arrayJSON.length(); i++) {
				JSONObject objectJSON = arrayJSON.getJSONObject(i);
				if (i == numOpera) {
					img = objectJSON.getString("img");
				}

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return img;

	}

	private boolean appInstalledOrNot(String uri) {
		PackageManager pm = getPackageManager();
		boolean app_installed = false;
		try {
			pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}

	// per ottenere l'ultimo colore vinto
	public int lastColor() {
		final SharedPreferences prefs = getSharedPreferences(MY_PREFERENCES,
				Context.MODE_PRIVATE);
		int lastColor = Color.TRANSPARENT;

		for (int i = 0; i <= mColors; i++) {

			if (prefs.contains("coloreScelto" + i)) {

				if (prefs.getInt("coloreScelto" + i, Color.TRANSPARENT) != -1)
					lastColor = prefs.getInt("coloreScelto" + i,
							Color.TRANSPARENT);
			}
		}

		if (mPaint.getColor() != Color.BLACK)
			lastColor = mPaint.getColor();

		return lastColor;
	}

}
