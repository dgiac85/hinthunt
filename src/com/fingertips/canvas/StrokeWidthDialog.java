package com.fingertips.canvas;

import com.fingertips.hinthunt.R;

import android.os.Bundle;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

public class StrokeWidthDialog extends Dialog implements
		OnSeekBarChangeListener {

	private Context c;

	public interface OnWidthChangedListener {
		void WidthChanged(int valore);
	}

	private OnWidthChangedListener Listener;

	public float width;
	public float inVal;

	public StrokeWidthDialog(Context c, OnWidthChangedListener listener,
			float init) {
		super(c);
		Listener = listener;
		inVal = init;
	}

	Button ok;
	TextView mProgressText;
	Paint mPaint;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setTitle(this.getContext().getResources().getString(R.string.Stroke1));
		setContentView(R.layout.stroke_width_dialog);

		SeekBar barra = (SeekBar) findViewById(R.id.barra);
		barra.setIndeterminate(false);
		barra.setProgress((int) inVal);
		barra.setOnSeekBarChangeListener(this);

		mProgressText = (TextView) findViewById(R.id.tv1);
		mProgressText.setText(this.getContext().getResources()
				.getString(R.string.Stroke2)
				+ barra.getProgress());

		ok = (Button) findViewById(R.id.b1);
		ok.setOnClickListener(new View.OnClickListener() {

			public void onClick(View v) {
				Listener.WidthChanged((int) width);
				dismiss();
			}
		});

	}

	public void onProgressChanged(SeekBar seekBar, final int progress,
			boolean fromTouch) {

		mProgressText.setText(this.getContext().getResources()
				.getString(R.string.Stroke2)
				+ progress);
		width = progress;

	}

	public void onStartTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}

	public void onStopTrackingTouch(SeekBar seekBar) {
		// TODO Auto-generated method stub

	}

}
