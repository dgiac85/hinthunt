package com.fingertips.canvas;

public class Colore {

	String stato;
	int codice;

	public Colore(String stato, int codice) {
		super();
		this.stato = stato;
		this.codice = codice;
	}

	public String getStato() {
		return stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

	public int getCodice() {
		return codice;
	}

	public void setCodice(int codice) {
		this.codice = codice;
	}

}
