package com.fingertips.canvas;

import com.fingertips.hinthunt.R;
import com.fingertips.hints.IndiziActivity;
import com.fingertips.utils.LoadingActivity2;

import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.*;
import android.graphics.drawable.AnimationDrawable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class ColorPickerDialog extends Dialog {

	public interface OnColorChangedListener {
		void colorChanged(int color);
	}

	private OnColorChangedListener mListener;
	private int mInitialColor;
	AnimationDrawable tutorial,tutorial_2;
	ImageView img_tutorial,img_tutorial_2;
	int posizione;
	Button[] Tavolozza;
	CanvasActivity ca;

	Context context;
	private final static String MY_PREFERENCES = "preferences";

	public ColorPickerDialog(Context context, OnColorChangedListener listener,
			int initialColor) {
		super(context);

		ca = (CanvasActivity) context;
		mListener = listener;
		mInitialColor = initialColor;
		this.context = context;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.colorpicker_dialog);

		img_tutorial = (ImageView) findViewById(R.id.imageView1);
		img_tutorial_2 = (ImageView) findViewById(R.id.imageView3);
		img_tutorial_2.setVisibility(View.INVISIBLE);
		img_tutorial.setBackgroundResource(R.drawable.tutorial_colors);
		img_tutorial_2.setBackgroundResource(R.drawable.tutorial_retry); 
		tutorial = (AnimationDrawable) img_tutorial.getBackground();
		tutorial_2 = (AnimationDrawable) img_tutorial_2.getBackground();

		final SharedPreferences prefs = getContext().getSharedPreferences(
				MY_PREFERENCES, Context.MODE_PRIVATE);
		if (((prefs.contains("DaRipetere0")) || (prefs.contains("DaRipetere1"))
				|| (prefs.contains("DaRipetere2"))
				|| (prefs.contains("DaRipetere3"))
				|| (prefs.contains("DaRipetere4"))
				|| (prefs.contains("DaRipetere5"))
				|| (prefs.contains("DaRipetere6")))&&(prefs.getBoolean("MostraTutorialRipeti", false)) ){
			img_tutorial_2.setVisibility(View.VISIBLE);
			tutorial_2.start();
			SharedPreferences.Editor editor = prefs.edit();
			editor.putBoolean("MostraTutorialRipeti",false);
			editor.commit();
		} else {
			tutorial.start();
		}

		Tavolozza = new Button[] { (Button) findViewById(R.id.color0),
				(Button) findViewById(R.id.color1),
				(Button) findViewById(R.id.color2),
				(Button) findViewById(R.id.color3),
				(Button) findViewById(R.id.color4),
				(Button) findViewById(R.id.color5),
				(Button) findViewById(R.id.color6) };

		if ((prefs.contains("DaRipetere0"))|| (prefs.contains("DaRipetere1"))
				|| (prefs.contains("DaRipetere2"))
				|| (prefs.contains("DaRipetere3"))
				|| (prefs.contains("DaRipetere4"))
				|| (prefs.contains("DaRipetere5"))
				|| (prefs.contains("DaRipetere6"))) {
			img_tutorial.setVisibility(View.INVISIBLE);
			
		} else {
			for (int i = 0; i <= 6; i++) {
				Tavolozza[i].setEnabled(false);
			}

		}
		
		/*if(!prefs.getBoolean("MostraTutorialRipeti", false)){
			img_tutorial_2.setVisibility(View.INVISIBLE);
		}
		else {
				for (int i = 0; i <= 6; i++) {
					Tavolozza[i].setEnabled(false);
				}

		}*/

		View.OnClickListener l = new View.OnClickListener() {
			public void onClick(View v) {

				Button b = (Button) v;

				if (b == (Button) findViewById(R.id.color0)) {
					posizione = 0;
				}

				if (b == (Button) findViewById(R.id.color1)) {
					posizione = 1;
				}

				if (b == (Button) findViewById(R.id.color2)) {
					posizione = 2;
				}

				if (b == (Button) findViewById(R.id.color3)) {
					posizione = 3;
				}

				if (b == (Button) findViewById(R.id.color4)) {
					posizione = 4;
				}

				if (b == (Button) findViewById(R.id.color5)) {
					posizione = 5;
				}

				if (b == (Button) findViewById(R.id.color6)) {
					posizione = 6;
				}

				if (b.getText().toString().equalsIgnoreCase("QR")) {

					SharedPreferences.Editor editor = prefs.edit();
					editor.putInt("posizione", posizione);

					editor.commit();

					String immagine = null;
					final ProgressDialog progressDialog;
					Intent i = new Intent(context, LoadingActivity2.class);

					dismiss();

					context.startActivity(i);
					((Activity) context).finish();

				}

				else if ((b.getText().toString().equalsIgnoreCase("1"))
						|| (b.getText().toString().equalsIgnoreCase("2"))
						|| (b.getText().toString().equalsIgnoreCase("3"))
						|| (b.getText().toString().equalsIgnoreCase("4"))
						|| (b.getText().toString().equalsIgnoreCase("5"))
						|| (b.getText().toString().equalsIgnoreCase("6"))
						|| (b.getText().toString().equalsIgnoreCase("7"))) {
					// torna alla stanza in cui avevi sbagliato

					String toStanza = (String) b.getText();
					SharedPreferences.Editor editor = prefs.edit();
					editor.putInt("posizione", posizione);
					editor.commit();

					final ProgressDialog progressDialog;
					Intent i = new Intent(context, IndiziActivity.class);
					i.putExtra("position", toStanza);
					dismiss();
					progressDialog = ProgressDialog.show(
							context,
							context.getResources().getString(
									R.string.loadingText),
							context.getResources().getString(
									R.string.title_activity_loading));

					new Thread() {

						public void run() {

							try {

								sleep(200);
							} catch (Exception e) {

								Log.e("tag", e.getMessage());

							}

							// dismiss the progress dialog

							progressDialog.dismiss();

						}

					}.start();

					context.startActivity(i);
					((Activity) context).finish();

				}

				else {
					int pickcolor = b.getCurrentTextColor();
					mListener.colorChanged(pickcolor);
					dismiss();
				}
			}
		};

		for (int x = 0; x < Tavolozza.length; x++) {

			// prendo il colore dalle shared preferences, con valore di defalut
			// a 0
			int color = prefs.getInt("coloreScelto" + x, 0);

			if (color == -1) {
				Tavolozza[x].getBackground().setColorFilter(Color.BLACK,
						PorterDuff.Mode.LIGHTEN);
			} else {
				Tavolozza[x].getBackground().setColorFilter(color,
						PorterDuff.Mode.LIGHTEN);
			}
			// il colore � ancora disponibile, rimando al QR
			if (color == 0) {
				Tavolozza[x].setText("QR");
				Tavolozza[x].setTextColor(Color.WHITE);
			}

			// se c'� gi� un colore --> dipingo
			if ((color != 0) && (color != -1)) {
				Tavolozza[x].setTextColor(color);
				Tavolozza[x].setText("paint");
			}

			// modificare l'if con color, settando un valore che identifica
			// colore errato
			if (color == -1) {

				Tavolozza[x].setTextColor(Color.WHITE);
				if (prefs.contains("DaRipetere" + x)) {
					String stanzaRip = prefs.getString("DaRipetere" + x, "0");
					Tavolozza[x].setText(stanzaRip);
				}
			}

			Tavolozza[x].setOnClickListener(l);

		}

		setTitle(context.getResources().getString(R.string.colorPicker1));
	}

	@Override
	public void onBackPressed() {

		mListener.colorChanged(ca.lastColor());
		super.onBackPressed();
	}

	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			tutorial_2.stop();
			img_tutorial_2.setVisibility(View.GONE);
			tutorial.stop();
			img_tutorial.setVisibility(View.GONE);
			for (int i = 0; i <= 6; i++) {
				Tavolozza[i].setEnabled(true);
			}

			return true;
		}
		return super.onTouchEvent(event);
	}

}
