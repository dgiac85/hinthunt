package com.fingertips.canvas;

public class LineToCmd extends PathCommand {

	private float x, y;

	public LineToCmd(float x, float y) {
		this.x = x;
		this.y = y;

	}

	public String saveC() {
		return "l" + "," + x + "," + y + ";";
	}

}
