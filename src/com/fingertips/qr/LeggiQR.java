/*
 * Basic no frills app which integrates the ZBar barcode scanner with
 * the camera.
 * 
 * Created by lisah0 on 2012-02-24
 */
package com.fingertips.qr;

import com.fingertips.canvas.CanvasActivity;

import com.fingertips.hinthunt.R;
import com.fingertips.hints.IndiziActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.Button;
import android.widget.Toast;

import android.graphics.Typeface;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Size;

import android.widget.TextView;

/* Import ZBar Class files */
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;
import net.sourceforge.zbar.Config;

public class LeggiQR extends Activity {

	boolean fai = false;

	private Camera mCamera;
	private CameraPreview mPreview;
	private Handler autoFocusHandler;
	private final static String MY_PREFERENCES = "preferences";
	TextView scanText;
	Button scanButton, newButton;
	Context c;
	public String msg;

	ImageScanner scanner;

	private boolean barcodeScanned = false;
	private boolean previewing = true;

	static {
		System.loadLibrary("iconv");
	}

	@SuppressWarnings("deprecation")
	public int getScreenOrientation() {
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int h = displaymetrics.heightPixels;
		int w = displaymetrics.widthPixels;
		int orientation = Configuration.ORIENTATION_UNDEFINED;
		if (w == h) {
			orientation = Configuration.ORIENTATION_SQUARE;
		} else {
			if (w < h) {
				orientation = Configuration.ORIENTATION_PORTRAIT;
			} else {
				orientation = Configuration.ORIENTATION_LANDSCAPE;
			}
		}
		return orientation;
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getScreenOrientation() == Configuration.ORIENTATION_PORTRAIT) {
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			setContentView(R.layout.activity_leggi_qr);
		}

		if (getScreenOrientation() == Configuration.ORIENTATION_LANDSCAPE) {
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
			setContentView(R.layout.qrland);
		}

		onDialogClose();

	}

	public void onDialogClose() {

		autoFocusHandler = new Handler();
		mCamera = getCameraInstance();

		// Instance barcode scanner
		scanner = new ImageScanner();
		scanner.setConfig(0, Config.X_DENSITY, 3);
		scanner.setConfig(0, Config.Y_DENSITY, 3);

		mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB);
		FrameLayout preview = (FrameLayout) findViewById(R.id.cameraPreview);

		preview.addView(mPreview);

		scanText = (TextView) findViewById(R.id.scanText);

		String fontPath = "fonts/Noteworthy.ttc";
		Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);

		scanButton = (Button) findViewById(R.id.ScanButton);

		scanButton.setTypeface(tf);

		scanButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (barcodeScanned) {
					barcodeScanned = false;
					scanText.setText("Scanning...");
					mCamera.setPreviewCallback(previewCb);
					mCamera.startPreview();
					previewing = true;
					mCamera.autoFocus(autoFocusCB);
				}
			}
		});
	}

	public void onPause() {
		super.onPause();
		releaseCamera();
		this.finish();
	}

	/** A safe way to get an instance of the Camera object. */
	public static Camera getCameraInstance() {
		Camera c = null;
		try {
			c = Camera.open();
		} catch (Exception e) {
		}
		return c;
	}

	private void releaseCamera() {
		if (mCamera != null) {
			previewing = false;
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
	}

	private Runnable doAutoFocus = new Runnable() {
		public void run() {
			if (previewing)
				mCamera.autoFocus(autoFocusCB);
		}
	};

	public String getMsg() {
		return msg;
	}

	void scanning() {
		autoFocusHandler = new Handler();
		mCamera = getCameraInstance();

		// Instance barcode scanner
		scanner = new ImageScanner();
		scanner.setConfig(0, Config.X_DENSITY, 3);
		scanner.setConfig(0, Config.Y_DENSITY, 3);

		mPreview = new CameraPreview(this, mCamera, previewCb, autoFocusCB);
		FrameLayout preview = (FrameLayout) findViewById(R.id.cameraPreview);
		preview.addView(mPreview);

		scanText = (TextView) findViewById(R.id.scanText);

		if (barcodeScanned) {
			barcodeScanned = false;
			scanText.setText("Scanning...");
			mCamera.setPreviewCallback(previewCb);
			mCamera.startPreview();
			previewing = true;
			mCamera.autoFocus(autoFocusCB);
		}
	}

	PreviewCallback previewCb = new PreviewCallback() {
		public void onPreviewFrame(byte[] data, Camera camera) {
			Camera.Parameters parameters = camera.getParameters();
			Size size = parameters.getPreviewSize();

			Image barcode = new Image(size.width, size.height, "Y800");
			barcode.setData(data);

			int result = scanner.scanImage(barcode);

			if (result != 0) {
				previewing = false;
				mCamera.setPreviewCallback(null);
				mCamera.stopPreview();

				SymbolSet syms = scanner.getResults();
				for (Symbol sym : syms) {

					msg = sym.getData();
					barcodeScanned = true;
				}

				final SharedPreferences settings1 = getSharedPreferences(
						MY_PREFERENCES, Context.MODE_PRIVATE);
				if ((msg.equalsIgnoreCase("stanza1"))
						|| (msg.equalsIgnoreCase("stanza2"))
						|| (msg.equalsIgnoreCase("stanza3"))
						|| (msg.equalsIgnoreCase("stanza4"))
						|| (msg.equalsIgnoreCase("stanza5"))
						|| (msg.equalsIgnoreCase("stanza6"))
						|| (msg.equalsIgnoreCase("stanza7"))
						|| (msg.equalsIgnoreCase("stanza8"))) {
					if (settings1.getBoolean("FinitoStanza" + msg.charAt(6),
							false)) {

						mostraDialogHaiGiaTerm();
						fai = false;
					} else {
						fai = true;
					}
				}

				if (fai) {
					fai = false;
					final AlertDialog.Builder ad = new AlertDialog.Builder(
							LeggiQR.this);
					ad.setTitle(getResources().getString(R.string.doveSei));
					if (msg.equalsIgnoreCase("stanza1")) {
						ad.setMessage(getResources().getString(
								R.string.StanzaUno));
					}
					if (msg.equalsIgnoreCase("stanza2")) {
						ad.setMessage(getResources().getString(
								R.string.StanzaDue));
					}
					if (msg.equalsIgnoreCase("stanza3")) {
						ad.setMessage(getResources().getString(
								R.string.StanzaTre));
					}
					if (msg.equalsIgnoreCase("stanza4")) {
						ad.setMessage(getResources().getString(
								R.string.StanzaQuattro));
					}
					if (msg.equalsIgnoreCase("stanza5")) {
						ad.setMessage(getResources().getString(
								R.string.StanzaCinque));
					}
					if (msg.equalsIgnoreCase("stanza6")) {
						ad.setMessage(getResources().getString(
								R.string.StanzaSei));
					}
					if (msg.equalsIgnoreCase("stanza7")) {
						ad.setMessage(getResources().getString(
								R.string.StanzaSette));
					}
					if (msg.equalsIgnoreCase("stanza8")) {
						ad.setMessage("Stanza dei quadri neri(8)");
					}

					final SharedPreferences settings = getSharedPreferences(
							MY_PREFERENCES, 0);
					ad.setPositiveButton(
							getResources().getString(R.string.avanti),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									String mess = getMsg();

									if (!mess.contains("stanza"))
										Toast.makeText(LeggiQR.this,
												"QRCode Error",
												Toast.LENGTH_SHORT).show();
									LeggiQR.this.finish();
									if (mess.equalsIgnoreCase("stanza1")) {

										if (!(settings.getBoolean("risolto1",
												false))) {
											Intent i = new Intent(
													getApplicationContext(),
													IndiziActivity.class);
											i.putExtra("position", "1");
											startActivity(i);
											LeggiQR.this.finish();
										} else {

											Toast.makeText(
													LeggiQR.this,
													getResources().getString(
															R.string.Already),
													Toast.LENGTH_LONG).show();
											Intent i = new Intent(
													getApplicationContext(),
													CanvasActivity.class);
											startActivity(i);
											LeggiQR.this.finish();

										}
									}

									if (mess.equalsIgnoreCase("stanza2")) {
										if (!(settings.getBoolean("risolto2",
												false))) {
											Intent i = new Intent(
													getApplicationContext(),
													IndiziActivity.class);
											i.putExtra("position", "2");
											startActivity(i);
											LeggiQR.this.finish();
										} else {

											Toast.makeText(
													LeggiQR.this,
													getResources().getString(
															R.string.Already),
													Toast.LENGTH_LONG).show();
											Intent i = new Intent(
													getApplicationContext(),
													CanvasActivity.class);
											startActivity(i);
											LeggiQR.this.finish();
										}
									}
									if (mess.equalsIgnoreCase("stanza3")) {
										if (!(settings.getBoolean("risolto3",
												false))) {
											Intent i = new Intent(
													getApplicationContext(),
													IndiziActivity.class);
											i.putExtra("position", "3");
											startActivity(i);
											LeggiQR.this.finish();
										} else {

											Toast.makeText(
													LeggiQR.this,
													getResources().getString(
															R.string.Already),
													Toast.LENGTH_LONG).show();
											Intent i = new Intent(
													getApplicationContext(),
													CanvasActivity.class);
											startActivity(i);
											LeggiQR.this.finish();

										}

									}
									if (mess.equalsIgnoreCase("stanza4")) {
										if (!(settings.getBoolean("risolto4",
												false))) {
											Intent i = new Intent(
													getApplicationContext(),
													IndiziActivity.class);
											i.putExtra("position", "4");
											startActivity(i);
											LeggiQR.this.finish();
										} else {

											Toast.makeText(
													LeggiQR.this,
													getResources().getString(
															R.string.Already),
													Toast.LENGTH_LONG).show();
											Intent i = new Intent(
													getApplicationContext(),
													CanvasActivity.class);
											startActivity(i);
											LeggiQR.this.finish();

										}

									}
									if (mess.equalsIgnoreCase("stanza5")) {
										if (!(settings.getBoolean("risolto5",
												false))) {
											Intent i = new Intent(
													getApplicationContext(),
													IndiziActivity.class);
											i.putExtra("position", "5");
											startActivity(i);
											LeggiQR.this.finish();
										} else {

											Toast.makeText(
													LeggiQR.this,
													getResources().getString(
															R.string.Already),
													Toast.LENGTH_LONG).show();
											Intent i = new Intent(
													getApplicationContext(),
													CanvasActivity.class);
											startActivity(i);
											LeggiQR.this.finish();

										}

									}
									if (mess.equalsIgnoreCase("stanza6")) {
										if (!(settings.getBoolean("risolto6",
												false))) {
											Intent i = new Intent(
													getApplicationContext(),
													IndiziActivity.class);
											i.putExtra("position", "6");
											startActivity(i);
											LeggiQR.this.finish();
										} else {
											((DialogInterface) ad).dismiss();
											progress();
											Toast.makeText(
													LeggiQR.this,
													getResources().getString(
															R.string.Already),
													Toast.LENGTH_LONG).show();
											Intent i = new Intent(
													getApplicationContext(),
													CanvasActivity.class);
											startActivity(i);
											LeggiQR.this.finish();

										}

									}
									if (mess.equalsIgnoreCase("stanza7")) {
										if (!(settings.getBoolean("risolto7",
												false))) {
											Intent i = new Intent(
													getApplicationContext(),
													IndiziActivity.class);
											i.putExtra("position", "7");
											startActivity(i);
											LeggiQR.this.finish();
										} else {

											Toast.makeText(
													LeggiQR.this,
													getResources().getString(
															R.string.Already),
													Toast.LENGTH_LONG).show();
											Intent i = new Intent(
													getApplicationContext(),
													CanvasActivity.class);
											startActivity(i);
											LeggiQR.this.finish();

										}

									}

								}
							});

					AlertDialog alert = ad.create();
					alert.show();
				}

			}
		}
	};

	// Mimic continuous auto-focusing
	AutoFocusCallback autoFocusCB = new AutoFocusCallback() {
		public void onAutoFocus(boolean success, Camera camera) {
			autoFocusHandler.postDelayed(doAutoFocus, 1000);
		}
	};

	Button scan;

	public void mostraDialogHaiGiaTerm() {

		final Dialog dialogInfo = new Dialog(LeggiQR.this);
		dialogInfo.setTitle(getResources().getString(R.string.attenzione));

		dialogInfo.setContentView(R.layout.info_qr);
		Button exit = (Button) dialogInfo.getWindow().findViewById(
				R.id.NuovaScan);
		TextView text = (TextView) dialogInfo.getWindow().findViewById(
				R.id.textError);

		String fontPath = "fonts/Noteworthy.ttc";
		Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
		exit.setTypeface(tf);
		text.setTypeface(tf);

		dialogInfo.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

		exit.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				if (barcodeScanned) {
					barcodeScanned = false;
					scanText.setText("Scanning...");
					mCamera.setPreviewCallback(previewCb);
					mCamera.startPreview();
					previewing = true;
					mCamera.autoFocus(autoFocusCB);
				}

				dialogInfo.dismiss();

			}
		});

		dialogInfo.show();
	}

	@Override
	public void onBackPressed() {

		LeggiQR.this.finish();
	}

	public void progress() {
		final ProgressDialog progressDialog = ProgressDialog.show(this,
				getResources().getString(R.string.loadingText), getResources()
						.getString(R.string.title_activity_loading));

		new Thread() {

			public void run() {

				try {

					sleep(500);
				} catch (Exception e) {

					Log.e("tag", e.getMessage());

				}

				// dismiss the progress dialog

				progressDialog.dismiss();
			}

		}.start();
	}

	@Override
	protected void onResume() {
		super.onResume();

	}

	@Override
	protected void onStop() {
		super.onStop();

	}

}
