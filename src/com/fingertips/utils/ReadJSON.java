package com.fingertips.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;

public class ReadJSON {

	public JSONArray leggiJSON(Context ctx, String file) throws JSONException {
		String jsonString = "";
		try {
			Writer writer = new StringWriter();
			InputStream is = ctx.getResources().getAssets().open(file);
			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is,
						"UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				is.close();
			}
			jsonString = writer.toString();
		} catch (IOException e) {
			jsonString = "[]";
		}
		JSONArray a = new JSONArray(jsonString);
		return a;
	}

}
