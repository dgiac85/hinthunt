package com.fingertips.utils;

import com.fingertips.hinthunt.R;
import com.fingertips.qr.LeggiQR;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

public class LoadingActivity2 extends Activity {

	// Introduce an delay
	private final int WAIT_TIME = 1500;
	TextView carica;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_loading2);

		String fontPath = "fonts/Noteworthy.ttc";
		Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
		carica = (TextView) findViewById(R.id.caricamento);
		carica.setTypeface(tf);

		findViewById(R.id.mainSpinner1).setVisibility(View.VISIBLE);

		new Handler().postDelayed(new Runnable() {

			public void run() {

				System.out.println("STO A CARICARE - ASPETTA UN PO'");
				/* Create an Intent that will start the ProfileData-Activity. */
				Intent mainIntent = new Intent(LoadingActivity2.this,
						LeggiQR.class);
				LoadingActivity2.this.startActivity(mainIntent);
				LoadingActivity2.this.finish();
			}
		}, WAIT_TIME);
	}
}
