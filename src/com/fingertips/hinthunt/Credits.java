package com.fingertips.hinthunt;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Typeface;
import android.widget.TextView;

public class Credits extends Activity {

	TextView textview1, textview2, textview3, textview4, textview5, textview6,
			textview7, textview8, textview9;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_credits);

		String fontPath = "fonts/Noteworthy.ttc";
		Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);

		textview1 = (TextView) findViewById(R.id.textView1);
		textview1.setTypeface(tf);

		textview2 = (TextView) findViewById(R.id.textView2);
		textview2.setTypeface(tf);

		textview3 = (TextView) findViewById(R.id.textView3);
		textview3.setTypeface(tf);

		textview4 = (TextView) findViewById(R.id.textView4);
		textview4.setTypeface(tf);

		textview5 = (TextView) findViewById(R.id.textView5);
		textview5.setTypeface(tf);

		textview6 = (TextView) findViewById(R.id.textView6);
		textview6.setTypeface(tf);

		textview7 = (TextView) findViewById(R.id.textView7);
		textview7.setTypeface(tf);

		textview8 = (TextView) findViewById(R.id.textView8);
		textview8.setTypeface(tf);

		textview9 = (TextView) findViewById(R.id.textView9);
		textview9.setTypeface(tf);
	}

}
