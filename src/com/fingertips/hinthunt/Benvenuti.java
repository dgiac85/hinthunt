package com.fingertips.hinthunt;

import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;

public class Benvenuti extends Activity {

	private final int WAIT_TIME = 1500;
	private final static String MY_PREFERENCES = "preferences";
	TextView saluti;
	Boolean test;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_benvenuti);

		String fontPath = "fonts/Noteworthy.ttc";
		Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
		saluti = (TextView) findViewById(R.id.Benvenuti);
		saluti.setTypeface(tf);

		SharedPreferences prefs = getSharedPreferences(MY_PREFERENCES,
				Context.MODE_PRIVATE);
		test = prefs.getBoolean("scelto", false);
		if (test == true)
			saluti.setText(R.string.hello_world2);

		new Handler().postDelayed(new Runnable() {

			public void run() {
				// Simulating a long running task

				/* Create an Intent that will start the Hint Hunt.activity */
				Intent mainIntent = new Intent(Benvenuti.this, HintHunt.class);
				startActivity(mainIntent);
				Benvenuti.this.finish();
			}
		}, WAIT_TIME);
	}

}
