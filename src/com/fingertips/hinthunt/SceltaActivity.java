package com.fingertips.hinthunt;

import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fingertips.hinthunt.R;
import com.fingertips.utils.LoadingActivity;
import com.fingertips.utils.ReadJSON;

import android.os.Bundle;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
//import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;

import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.DecelerateInterpolator;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class SceltaActivity extends Activity {
	JSONArray arrayJSON = null;
	GridView gridview;
	TextView testo;
	int pos;
	private final static String MY_PREFERENCES = "preferences";
	private Animator mCurrentAnimator;
	private int mShortAnimationDuration;
	ImageView expandedImageView;
	Button pad1, pad2, pad3;
	TextView scegli;
	TextView regola;
	String lingua = null;
	Typeface tf;
	// sono i limiti dell'ingrandimento dell'anteprima in base allo schermo
	// dispositivo utilizzato
	final Rect startBounds = new Rect();
	final Rect finalBounds = new Rect();
	final Point globalOffset = new Point();
	float startScaleFinal;
	View thumbView;
	private ProgressDialog progressDialog = null;
	ArrayList<Integer> itemList = new ArrayList<Integer>();

	public class ImageAdapter extends BaseAdapter {

		private Context mContext;
		private LayoutInflater layoutInflater;
		ArrayList<String> itemList = new ArrayList<String>();

		public ImageAdapter(SceltaActivity activity) {
			layoutInflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			mContext = activity.getApplicationContext();
		}

		void add(String path) {
			itemList.add(path);
		}

		public int getCount() {
			return itemList.size();
		}

		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			View listItem = convertView;
			int pos = position;
			// imposto mediante un inflater gli elementi che andranno a popolare
			// una grid view
			if (listItem == null) {

				listItem = layoutInflater.inflate(R.layout.grid_item, null);
			}
			ImageView imageView = (ImageView) listItem.findViewById(R.id.thumb);
			String uri = itemList.get(pos);

			int immagine = getResources().getIdentifier(uri, null,
					getPackageName());

			imageView.setBackgroundResource(immagine);

			return listItem;

		}

	}

	ImageAdapter myImageAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scelta);
		String fontPath = "fonts/Noteworthy.ttc";
		tf = Typeface.createFromAsset(getAssets(), fontPath);

		scegli = (TextView) findViewById(R.id.scegli);
		scegli.setTypeface(tf);

		pad1 = (Button) findViewById(R.id.pad1);
		pad1.setTypeface(tf);

		pad2 = (Button) findViewById(R.id.pad2);
		pad2.setTypeface(tf);

		pad3 = (Button) findViewById(R.id.pad3);
		pad3.setTypeface(tf);
		gridview = (GridView) findViewById(R.id.gridView);
		myImageAdapter = new ImageAdapter(this);
		gridview.setAdapter(myImageAdapter);

		pad1 = (Button) findViewById(R.id.pad1);
		pad2 = (Button) findViewById(R.id.pad2);
		pad3 = (Button) findViewById(R.id.pad3);
		pad1.setVisibility(View.INVISIBLE);
		pad2.setVisibility(View.INVISIBLE);
		pad3.setVisibility(View.INVISIBLE);

		lingua = Locale.getDefault().getDisplayLanguage();
		ReadJSON obj = new ReadJSON();

		try {
			if (lingua.equalsIgnoreCase("Italiano")) {
				arrayJSON = obj.leggiJSON(this, "quadriPerScelta.json");
			}
			if (lingua.equalsIgnoreCase("English")) {
				arrayJSON = obj.leggiJSON(this, "quadriPerScelta_en.json");
			}
			for (int i = 0; i < arrayJSON.length(); i++) {
				JSONObject objectJSON = arrayJSON.getJSONObject(i);
				String immagine = objectJSON.getString("thumb");
				myImageAdapter.add(immagine);
				int intImmagine = getResources().getIdentifier(immagine, null,
						getPackageName());
				itemList.add(i, intImmagine);

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// gestione del listener della grid view
		gridview.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v,
					int position, long id) {
				pos = position;
				zoomImageFromThumb(v, itemList.get(pos), pos);
				pad1.setVisibility(View.VISIBLE);
				pad2.setVisibility(View.VISIBLE);
				pad3.setVisibility(View.VISIBLE);
				gridview.setEnabled(false);

				// gestione dei listener dei bottoni pad1, 2, 3
				pad1.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {

						DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog,
									int which) {

								switch (which) {
								case DialogInterface.BUTTON_POSITIVE:

									String immagine = null;
									SharedPreferences prefs = getSharedPreferences(
											MY_PREFERENCES,
											Context.MODE_PRIVATE);
									SharedPreferences.Editor editor = prefs
											.edit();
									editor.putBoolean("scelto", true);
									editor.putInt("numeroquadro", pos);
									editor.commit();

									dialog.dismiss();

									Intent i = new Intent(SceltaActivity.this,
											LoadingActivity.class);

									startActivity(i);

									SceltaActivity.this.finish();
									break;

								case DialogInterface.BUTTON_NEGATIVE:
									dialog.dismiss();
									break;
								}
							}
						};

						Builder builder = new AlertDialog.Builder(
								SceltaActivity.this);
						builder.setMessage(R.string.sicuro2)
								.setPositiveButton(R.string.si,
										dialogClickListener)
								.setNegativeButton(R.string.no,
										dialogClickListener).show();
					}
				});

			}
		});

		mShortAnimationDuration = getResources().getInteger(
				android.R.integer.config_shortAnimTime);

		// serve a chiudere l'anteprima ingrandita
		pad2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				chiusura();
			}
		});

		// mostra le info
		pad3.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				mostraDialogInfo(pos);
			}
		});

	}

	public void mostraDialogInfo(int position) {
		final Dialog dialogInfo = new Dialog(SceltaActivity.this);
		dialogInfo.setTitle(R.string.infoQuadro);

		dialogInfo.setContentView(R.layout.info_quadri);
		Button exit = (Button) dialogInfo.getWindow().findViewById(
				R.id.closeInfo);
		TextView annot = (TextView) dialogInfo.getWindow().findViewById(
				R.id.anno);
		TextView titolot = (TextView) dialogInfo.getWindow().findViewById(
				R.id.titolo);
		TextView tecnicat = (TextView) dialogInfo.getWindow().findViewById(
				R.id.tecnica);
		TextView stanzat = (TextView) dialogInfo.getWindow().findViewById(
				R.id.stanza);
		annot.setTypeface(tf);
		titolot.setTypeface(tf);
		tecnicat.setTypeface(tf);
		stanzat.setTypeface(tf);
		exit.setTypeface(tf);
		ImageView imageInfo = (ImageView) dialogInfo.getWindow().findViewById(
				R.id.imageInfo);
		dialogInfo.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

		ReadJSON obj = new ReadJSON();

		try {
			if (lingua.equalsIgnoreCase("Italiano")) {
				arrayJSON = obj.leggiJSON(this, "quadriPerScelta.json");
			}
			if (lingua.equalsIgnoreCase("English")) {
				arrayJSON = obj.leggiJSON(this, "quadriPerScelta_en.json");
			}

			for (int i = 0; i < arrayJSON.length(); i++) {
				JSONObject objectJSON = arrayJSON.getJSONObject(i);
				if (i == position) {
					String immagine = objectJSON.getString("thumb");
					String anno = objectJSON.getString("anno");
					String titolo = objectJSON.getString("titolo");
					String tecnica = objectJSON.getString("tecnica");
					String stanza = objectJSON.getString("stanza");
					Spanned spannable1 = Html.fromHtml(anno);
					Spanned spannable2 = Html.fromHtml(titolo);
					Spanned spannable3 = Html.fromHtml(tecnica);
					Spanned spannable4 = Html.fromHtml(stanza);

					annot.setText(getApplicationContext().getResources().getString(R.string.Anno) + ":" + spannable1);
					titolot.setText(spannable2);
					stanzat.setText(getApplicationContext().getResources().getString(R.string.Tecnica) + ": " + spannable3);
					tecnicat.setText(getApplicationContext().getResources().getString(R.string.Stanza) + ":" + spannable4);

					// settare l'immagine vista all'interno della dialog del
					// quadro scelto
					int intImmagine = getResources().getIdentifier(immagine,
							null, getPackageName());
					imageInfo.setImageResource(intImmagine);
				}

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		exit.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				dialogInfo.dismiss();
			}
		});

		dialogInfo.show();
	}

	private void zoomImageFromThumb(final View thumbView, int imageResId,
			int pos) {
		this.thumbView = thumbView;
		if (mCurrentAnimator != null) {
			mCurrentAnimator.cancel();
		}
		ReadJSON obj2 = new ReadJSON();
		// Load the high-resolution "zoomed-in" image.
		expandedImageView = (ImageView) findViewById(R.id.expanded_image);
		// expandedImageView.setImageResource(imageResId);

		String immagine = null;
		String testo1 = null;
		try {
			if (lingua.equalsIgnoreCase("Italiano")) {
				arrayJSON = obj2.leggiJSON(this, "quadriPerScelta.json");
			}
			if (lingua.equalsIgnoreCase("English")) {
				arrayJSON = obj2.leggiJSON(this, "quadriPerScelta_en.json");
			}

			for (int i = 0; i < arrayJSON.length(); i++) {
				JSONObject objectJSON = arrayJSON.getJSONObject(i);
				if (i == pos) {
					immagine = objectJSON.getString("img");

				}
				// dialog1.setTitle(testo1);

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int immagine1 = getResources().getIdentifier(immagine, null,
				getPackageName());
		expandedImageView.setImageResource(immagine1);
		// Calculate the starting and ending bounds for the zoomed-in image.
		// This step
		// involves lots of math. Yay, math.

		// The start bounds are the global visible rectangle of the thumbnail,
		// and the
		// final bounds are the global visible rectangle of the container view.
		// Also
		// set the container view's offset as the origin for the bounds, since
		// that's
		// the origin for the positioning animation properties (X, Y).
		thumbView.getGlobalVisibleRect(startBounds);
		findViewById(R.id.container).getGlobalVisibleRect(finalBounds,
				globalOffset);
		startBounds.offset(-globalOffset.x, -globalOffset.y);
		finalBounds.offset(-globalOffset.x, -globalOffset.y);

		// Adjust the start bounds to be the same aspect ratio as the final
		// bounds using the
		// "center crop" technique. This prevents undesirable stretching during
		// the animation.
		// Also calculate the start scaling factor (the end scaling factor is
		// always 1.0).
		float startScale;
		if ((float) finalBounds.width() / finalBounds.height() > (float) startBounds
				.width() / startBounds.height()) {
			// Extend start bounds horizontally
			startScale = (float) startBounds.height() / finalBounds.height();
			float startWidth = startScale * finalBounds.width();
			float deltaWidth = (startWidth - startBounds.width()) / 2;
			startBounds.left -= deltaWidth;
			startBounds.right += deltaWidth;
		} else {
			// Extend start bounds vertically
			startScale = (float) startBounds.width() / finalBounds.width();
			float startHeight = startScale * finalBounds.height();
			float deltaHeight = (startHeight - startBounds.height()) / 2;
			startBounds.top -= deltaHeight;
			startBounds.bottom += deltaHeight;
		}
		Button b;
		// Hide the thumbnail and show the zoomed-in view. When the animation
		// begins,
		// it will position the zoomed-in view in the place of the thumbnail.
		// thumbView.setAlpha(0f);
		expandedImageView.setVisibility(View.VISIBLE);

		/*
		 * b=(Button)findViewById(R.id.button1); b.setVisibility(View.VISIBLE);
		 */

		// Set the pivot point for SCALE_X and SCALE_Y transformations to the
		// top-left corner of
		// the zoomed-in view (the default is the center of the view).
		expandedImageView.setPivotX(0f);
		expandedImageView.setPivotY(0f);

		// Construct and run the parallel animation of the four translation and
		// scale properties
		// (X, Y, SCALE_X, and SCALE_Y).
		GridView grid = (GridView) findViewById(R.id.gridView);
		grid.setClickable(false);
		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		System.out.println(currentapiVersion);
		if (currentapiVersion > android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			// grid.setAlpha(0.1f);

			expandedImageView.setBackgroundColor(Color.argb(127, 0, 0, 0));
			// expandedImageView.setAlpha(1f);
			// Do something for froyo and above versions
			AnimatorSet set = new AnimatorSet();
			set.play(
					ObjectAnimator.ofFloat(expandedImageView, View.X,
							startBounds.left, finalBounds.left))
					.with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
							startBounds.top, finalBounds.top))
					.with(ObjectAnimator.ofFloat(expandedImageView,
							View.SCALE_X, startScale, 1f))
					.with(ObjectAnimator.ofFloat(expandedImageView,
							View.SCALE_Y, startScale, 1f))
					.with(ObjectAnimator.ofFloat(expandedImageView, View.ALPHA,
							0, 1));
			set.setDuration(mShortAnimationDuration);
			set.setInterpolator(new DecelerateInterpolator());
			set.addListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mCurrentAnimator = null;
				}

				@Override
				public void onAnimationCancel(Animator animation) {
					mCurrentAnimator = null;
				}
			});
			set.start();
			mCurrentAnimator = set;
		}

		// Upon clicking the zoomed-in image, it should zoom back down to the
		// original bounds
		// and show the thumbnail instead of the expanded image.
		startScaleFinal = startScale;
		expandedImageView.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				chiusura();
			}
		});

	}

	void chiusura() {
		gridview.setEnabled(true);
		pad1.setVisibility(View.INVISIBLE);
		pad2.setVisibility(View.INVISIBLE);
		pad3.setVisibility(View.INVISIBLE);
		if (mCurrentAnimator != null) {
			mCurrentAnimator.cancel();

		}

		// Animate the four positioning/sizing properties in parallel,
		// back to their
		// original values.
		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		if (currentapiVersion > android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			AnimatorSet set = new AnimatorSet();
			set.play(
					ObjectAnimator.ofFloat(expandedImageView, View.X,
							startBounds.left))
					.with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
							startBounds.top))
					.with(ObjectAnimator.ofFloat(expandedImageView,
							View.SCALE_X, startScaleFinal))
					.with(ObjectAnimator.ofFloat(expandedImageView,
							View.SCALE_Y, startScaleFinal))
					.with(ObjectAnimator.ofFloat(expandedImageView, View.ALPHA,
							1, 0));
			set.setDuration(mShortAnimationDuration);
			set.setInterpolator(new DecelerateInterpolator());
			set.addListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					// thumbView.setAlpha(1f);
					expandedImageView.setVisibility(View.GONE);
					mCurrentAnimator = null;
				}

				@Override
				public void onAnimationCancel(Animator animation) {
					// thumbView.setAlpha(1f);
					expandedImageView.setVisibility(View.GONE);

					mCurrentAnimator = null;
				}
			});
			set.start();
			mCurrentAnimator = set;
		} else {
			thumbView.setAlpha(1f);
			expandedImageView.setVisibility(View.GONE);

		}
	}
}
