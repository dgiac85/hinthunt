package com.fingertips.hinthunt;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import com.fingertips.gallery.MappaActivity;

import com.fingertips.hinthunt.R;
import com.fingertips.utils.LoadingActivity;

import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


public class HintHunt extends Activity {

	Button entra;
	Button galleria;
	boolean test = false;
	boolean test2 = false;

	private final static String MY_PREFERENCES = "preferences";
	public static boolean firstUse;
	public static Bitmap saveSfondo = null;
	FileOutputStream store = null;
	FileInputStream restore = null;
	Button entra2, galleria2, help2;
	private ProgressDialog progressDialog;

	@Override
	protected void onPause() {
		super.onPause();
		Log.i(this.getClass().toString(), "Richiamato onPause()");
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		Log.i(this.getClass().toString(), "Richiamato onRestart()");
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.i(this.getClass().toString(), "Richiamato onStart()");
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.i(this.getClass().toString(), "Richiamato onResume()");

		SharedPreferences prefs = getSharedPreferences(MY_PREFERENCES,
				Context.MODE_PRIVATE);
		test = prefs.getBoolean("scelto", false);

		if (test == true)
			entra2.setText(R.string.continua);
		test2 = prefs.getBoolean("controllo", false);
		Log.i(this.getClass().toString(), "Richiamato onResume()");
	}

	@Override
	protected void onStop() {
		super.onStop();

		if (saveSfondo != null) {
			try {
				saveBS();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Log.i(this.getClass().toString(), "Richiamato onStop()");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.i(this.getClass().toString(), "Richiamato onDestroy()");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_hint_hunt);
		SharedPreferences prefs = getSharedPreferences(MY_PREFERENCES,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();

		firstUse = true;

		// font
		String fontPath = "fonts/Noteworthy.ttc";
		Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);

		entra2 = (Button) findViewById(R.id.entra);
		entra2.setTypeface(tf);

		galleria2 = (Button) findViewById(R.id.galleria);
		galleria2.setTypeface(tf);

		help2 = (Button) findViewById(R.id.help);
		help2.setTypeface(tf);

		test = prefs.getBoolean("scelto", false);

		test2 = prefs.getBoolean("controllo", false);
		entra = (Button) findViewById(R.id.entra);
		galleria = (Button) findViewById(R.id.galleria);

		if (test == true) {

			entra2.setText(R.string.continua);
		}

		galleria.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Intent i = new Intent(HintHunt.this, MappaActivity.class);
				startActivity(i);
			}
		});

		help2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(HintHunt.this, Credits.class);
				startActivity(i);
			}
		});

		entra.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// controllo per l'accesso alle activity.
				// se test � false --> vado a scelta activity
				// se test � true --> vado a loading activity
				boolean test = false;
				int quadroscelto;
				SharedPreferences prefs = getSharedPreferences(MY_PREFERENCES,
						Context.MODE_PRIVATE);
				test = prefs.getBoolean("scelto", false);
				quadroscelto = prefs.getInt("numeroquadro", -1);

				if (test == false) {
					String sottotitolo = getResources().getString(
							R.string.loadingText);
					String titolo = getResources().getString(
							R.string.title_activity_loading);
					progressDialog = ProgressDialog.show(HintHunt.this, titolo,
							sottotitolo);

					// feedback utente
					new Thread() {

						public void run() {

							try {

								sleep(1000);
							} catch (Exception e) {

								Log.e("tag", e.getMessage());

							}

							// dismiss the progress dialog

							progressDialog.dismiss();

						}

					}.start();

					Intent i = new Intent(HintHunt.this, SceltaActivity.class);
					startActivity(i);

				} else {

					Intent i = new Intent(HintHunt.this, LoadingActivity.class);
					startActivity(i);

				}

			}

		});

	}

	public void saveBS() throws IOException {
		// salva la bitmap selezionata per impostarla come sfondo della canvas
		store = openFileOutput("saveSfondo.png", MODE_WORLD_READABLE);
		OutputStreamWriter osw = new OutputStreamWriter(store);
		saveSfondo.compress(Bitmap.CompressFormat.PNG, 100, store);

		store.close();
		osw.close();

	}

	public void loadBS() throws IOException {

		restore = openFileInput("saveSfondo.png");
		saveSfondo = BitmapFactory.decodeStream(restore).copy(
				Bitmap.Config.ARGB_8888, true);

		restore.close();

	}

}
