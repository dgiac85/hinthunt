package com.fingertips.gallery;

import com.fingertips.hinthunt.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.res.Configuration;
import android.util.DisplayMetrics;
import android.widget.FrameLayout;

public class MappaActivity extends Activity {

	MView d;
	int w, h;
	FrameLayout preview;
	DisplayMetrics dm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mappa);

		dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		h = dm.heightPixels;
		w = dm.widthPixels;

		d = new MView(this, w, h);
		preview = (FrameLayout) findViewById(R.id.paint);
		preview.addView(d);

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		onDestroy();
		onCreate(null);

	}

}
