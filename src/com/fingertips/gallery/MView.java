package com.fingertips.gallery;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.fingertips.hinthunt.R;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.graphics.Paint.Align;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

public class MView extends View {

	private static final float TOUCH_TOLERANCE = 4;
	private Bitmap map;
	private Bitmap button;
	private Canvas mapCanvas;
	private int bw, bh, mw, mh;
	private float size;
	private Matrix m;
	private ScaleGestureDetector sgd;
	private float x, y;
	private ArrayList<Point> pArray;
	private Paint tempaint;
	private Paint normalpaint;
	private Paint alpha;
	private float attorno;
	private float limit;
	private long lastPinch;
	private InputStream Stream = null;

	public MView(Context c, int w, int h) {
		super(c);

		try {
			Stream = this.getResources().getAssets().open("stanze/map.jpg");
			map = BitmapFactory.decodeStream(Stream);
			Stream.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		limit = 0.75f;
		size = 1f;
		m = new Matrix();
		m.setScale(size, size);

		x = 0;
		y = 0;

		pArray = new ArrayList<Point>();

		pArray.add(new Point(480, 419));
		pArray.add(new Point(480, 219));
		pArray.add(new Point(338, 215));
		pArray.add(new Point(338, 419));
		pArray.add(new Point(178, 243));
		pArray.add(new Point(304, 90));
		pArray.add(new Point(471, 90));

		pArray.add(new Point(902, 166));
		pArray.add(new Point(1045, 133));
		pArray.add(new Point(1051, 320));
		pArray.add(new Point(1051, 445));

		float aspect = (float) map.getWidth() / (float) map.getHeight();

		bw = map.getWidth();
		bh = map.getHeight();
		mw = w;
		mh = h;

		button = Bitmap
				.createScaledBitmap(BitmapFactory.decodeResource(
						getResources(), R.drawable.laso_s), mw / 12, mw / 12,
						true);

		if (mw > mh)
			map = Bitmap.createScaledBitmap(map, (int) (mh * aspect / size),
					(int) (mh / size), true);

		else
			map = Bitmap.createScaledBitmap(map, (int) (mw / size), (int) (mw
					/ aspect / size), true);

		for (int i = 0; i < pArray.size(); i++) {

			pArray.get(i).x = (int) ((float) pArray.get(i).x / (float) bw * (float) map
					.getWidth());
			pArray.get(i).y = (int) ((float) pArray.get(i).y / (float) bh * (float) map
					.getHeight());

		}

		mapCanvas = new Canvas(map);

		String fontPath = "fonts/Noteworthy.ttc";
		Typeface tf = Typeface.createFromAsset(c.getAssets(), fontPath);

		tempaint = new Paint();
		tempaint.setColor(Color.WHITE);
		tempaint.setTypeface(tf);
		tempaint.setStyle(Paint.Style.FILL_AND_STROKE);
		tempaint.setAntiAlias(true);
		tempaint.setTextSize(button.getScaledWidth(mapCanvas) / 2);
		tempaint.setTextAlign(Align.CENTER);
		tempaint.setStrokeWidth(4);
		normalpaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		alpha = new Paint(Paint.ANTI_ALIAS_FLAG);
		alpha.setAlpha(96);

		attorno = button.getScaledWidth(mapCanvas) / 2;

		sgd = new ScaleGestureDetector(getContext(),
				new ScaleGestureDetector.OnScaleGestureListener() {

					PointF center = new PointF();

					public void onScaleEnd(ScaleGestureDetector detector) {

						// scaling=false;
						lastPinch = System.currentTimeMillis();
						invalidate();

					}

					public boolean onScaleBegin(ScaleGestureDetector detector) {

						// scaling=true;
						center.x = detector.getFocusX();
						center.y = detector.getFocusY();
						invalidate();

						return true;
					}

					public boolean onScale(ScaleGestureDetector detector) {
						float cx = detector.getFocusX();
						float cy = detector.getFocusY();
						float f = detector.getScaleFactor();
						float ox = (cx / size + x);
						float oy = (cy / size + y);

						size = Math.min(2, f * size);
						size = Math.max(0.5f, size);

						x = ox - cx / size - (cx - center.x) / size;
						y = oy - cy / size - (cy - center.y) / size;

						if (x <= -mw * limit / size)
							x = -mw * limit / size + 1;
						else if (x >= map.getWidth() * limit)
							x = map.getWidth() * limit;

						if (y <= -mh * limit / size)
							y = -mh * limit / size + 1;
						else if (y >= map.getHeight() * limit)
							y = map.getHeight() * limit;

						center.x = cx;
						center.y = cy;

						m.setScale(size, size);
						m.postTranslate(-x * size, -y * size);

						float pts[] = new float[] { ox, oy };
						m.mapPoints(pts);

						invalidate();

						return true;
					}
				});

	}

	@Override
	protected void onDraw(Canvas canvas) {

		canvas.drawBitmap(map, m, normalpaint);
		canvas.save();
		canvas.concat(m);
		for (int i = 0; i < pArray.size(); i++) {

			canvas.drawText("" + (i + 1), pArray.get(i).x, pArray.get(i).y
					+ tempaint.descent(), tempaint);

			if (i == pressed)
				canvas.drawBitmap(button, pArray.get(i).x - attorno,
						pArray.get(i).y - attorno, alpha);
			else
				canvas.drawBitmap(button, pArray.get(i).x - attorno,
						pArray.get(i).y - attorno, normalpaint);

			canvas.drawText("" + (i + 1), pArray.get(i).x, pArray.get(i).y
					+ tempaint.descent(), tempaint);
		}

		canvas.restore();

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {

		sgd.onTouchEvent(event);

		if (event.getPointerCount() == 1
				&& System.currentTimeMillis() - lastPinch > 200) {
			switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:

				touch_start(event.getX(), event.getY());

				invalidate();
				break;

			case MotionEvent.ACTION_MOVE:
				touch_move(event.getX(), event.getY());

				invalidate();
				break;

			case MotionEvent.ACTION_UP:

				touch_up();

				invalidate();
				break;
			}
		}
		return true;

	}

	private float mX, mY;
	private float realx, realy;
	boolean moved = false;

	int pressed = -1;

	private void touch_start(float ex, float ey) {

		mX = ex;
		mY = ey;
		moved = false;

		for (int i = 0; i < pArray.size(); i++) {
			if (mX / size + x > pArray.get(i).x - attorno
					&& mX / size + x < pArray.get(i).x + attorno
					&& mY / size + y > pArray.get(i).y - attorno
					&& mY / size + y < pArray.get(i).y + attorno) {
				pressed = i;

			}
		}

	}

	private void touch_move(float ex, float ey) {

		x -= (ex - mX) / size;
		y -= (ey - mY) / size;

		if (x <= -this.getWidth() * limit / size)
			x = -this.getWidth() * limit / size + 1;
		else if (x >= map.getWidth() * limit)
			x = map.getWidth() * limit;

		if (y <= -this.getHeight() * limit / size)
			y = -this.getHeight() * limit / size + 1;
		else if (y >= map.getHeight() * limit)
			y = map.getHeight() * limit;

		m.setScale(size, size);
		m.postTranslate(-x * size, -y * size);

		float dx = Math.abs(ex - mX);
		float dy = Math.abs(ey - mY);

		if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
			moved = true;
			pressed = -1;
		}

		mX = ex;
		mY = ey;

	}

	private void touch_up() {

		pressed = -1;
		if (!moved) {
			realx = mX / size + x;
			realy = mY / size + y;
			for (int i = 0; i < pArray.size(); i++) {
				if (realx > pArray.get(i).x - attorno
						&& realx < pArray.get(i).x + attorno
						&& realy > pArray.get(i).y - attorno
						&& realy < pArray.get(i).y + attorno) {

					System.out.println("toccatoepremuto " + i);
					Intent intent = new Intent();
					intent.setClass(this.getContext(), ViewPagerActivity.class);
					intent.putExtra("position", i + 1);
					this.getContext().startActivity(intent);
				}
			}
		}

	}

}