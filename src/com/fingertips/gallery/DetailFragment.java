package com.fingertips.gallery;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fingertips.hinthunt.R;
import com.fingertips.utils.ReadJSON;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Button;
import android.text.Html;
import android.text.Spanned;

import com.pagesuite.flowtext.FlowTextView;

public class DetailFragment extends Fragment {

	private Animator mCurrentAnimator;
	private int mShortAnimationDuration;
	FlowTextView tv;
	private int imageResourceId;
	private String description;
	private int position;
	private String dettagli;
	private Context ctx;
	private int numStanza;
	String packName;
	View view1;
	Button cresci;
	Button rimpicci;
	TextView testoOperaBig;
	TextView textNumStanza;
	LinearLayout ll, ll2;

	public DetailFragment(int imageResourceId, String dettagli,
			String description, int position, Context ctx, int numStanza,
			String packName) {
		this.imageResourceId = imageResourceId;
		this.description = description;
		this.position = position;
		this.dettagli = dettagli;
		this.numStanza = numStanza;
		this.ctx = ctx;
		this.packName = packName;

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e("Test", "hello");

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		String fontPath = "fonts/Noteworthy.ttc";
		Typeface tf = Typeface.createFromAsset(getResources().getAssets(),
				fontPath);

		view1 = inflater.inflate(R.layout.detailsgallery, container, false);
		cresci = (Button) view1.findViewById(R.id.cresci);
		ll = (LinearLayout) view1.findViewById(R.id.head1);
		textNumStanza = (TextView) view1.findViewById(R.id.numStanza);
		ll2 = (LinearLayout) view1.findViewById(R.id.head2);
		rimpicci = (Button) view1.findViewById(R.id.rimpicci);

		testoOperaBig = (TextView) view1.findViewById(R.id.operaGrande);
		mShortAnimationDuration = getResources().getInteger(
				android.R.integer.config_shortAnimTime);
		tv = (FlowTextView) view1.findViewById(R.id.textDetails);
		Spanned spannable = Html.fromHtml(dettagli);
		tv.setText(spannable);
		tv.setTextSize(28);

		tv.invalidate();

		textNumStanza.setText(getResources().getString(R.string.Stanza)+ " " + numStanza);
		textNumStanza.setTextSize(22);

		cresci.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				increase(view1);
			}
		});

		rimpicci.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				decrease(view1);
			}
		});

		ImageView imageView = (ImageView) view1.findViewById(R.id.detailsImage);
		imageView.setBackgroundResource(imageResourceId);
		imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
		imageView.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				ll.setVisibility(view1.INVISIBLE);
				ll2.setVisibility(view1.VISIBLE);

				zoomImageFromThumb(view1, numStanza, position);

			}
		});

		testoOperaBig.setTextColor(Color.BLACK);
		testoOperaBig.setTextSize(18);
		testoOperaBig.setText(description);

		final int pos = position + 1;

		return view1;
	}

	int textSize = 28;

	public void increase(View v) {

		if (textSize < 60) {
			textSize += 1;
			tv.setTextSize(textSize);
		}
	}

	public void decrease(View v) {

		if (textSize > 20) {
			textSize -= 1;
			tv.setTextSize(textSize);
		}

	}

	int immagine;
	JSONArray arrayJSON = null;

	private void zoomImageFromThumb(final View thumbView, int pos, int img) {
		// If there's an animation in progress, cancel it immediately and
		// proceed with this one.
		if (mCurrentAnimator != null) {
			mCurrentAnimator.cancel();
		}
		ReadJSON obj = new ReadJSON();
		try {
			arrayJSON = obj.leggiJSON(ctx, "stanze/stanza" + pos
					+ "/quadrigallery.json");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// Load the high-resolution "zoomed-in" image.
		final ImageView expandedImageView = (ImageView) thumbView
				.findViewById(R.id.expanded_image2);

		try {
			for (int i = 0; i < arrayJSON.length(); i++) {
				JSONObject objectJSON = arrayJSON.getJSONObject(i);
				String uri = objectJSON.getString("drawable");
				if (i == img) {
					immagine = getResources()
							.getIdentifier(uri, null, packName);

				}

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		expandedImageView.setImageResource(immagine);
		// Calculate the starting and ending bounds for the zoomed-in image.
		// This step
		// involves lots of math. Yay, math.
		final Rect startBounds = new Rect();
		final Rect finalBounds = new Rect();
		final Point globalOffset = new Point();

		// The start bounds are the global visible rectangle of the thumbnail,
		// and the
		// final bounds are the global visible rectangle of the container view.
		// Also
		// set the container view's offset as the origin for the bounds, since
		// that's
		// the origin for the positioning animation properties (X, Y).
		thumbView.getGlobalVisibleRect(startBounds);
		thumbView.findViewById(R.id.container2).getGlobalVisibleRect(
				finalBounds, globalOffset);
		startBounds.offset(-globalOffset.x, -globalOffset.y);
		finalBounds.offset(-globalOffset.x, -globalOffset.y);

		// Adjust the start bounds to be the same aspect ratio as the final
		// bounds using the
		// "center crop" technique. This prevents undesirable stretching during
		// the animation.
		// Also calculate the start scaling factor (the end scaling factor is
		// always 1.0).
		float startScale;
		if ((float) finalBounds.width() / finalBounds.height() > (float) startBounds
				.width() / startBounds.height()) {
			// Extend start bounds horizontally
			startScale = (float) startBounds.height() / finalBounds.height();
			float startWidth = startScale * finalBounds.width();
			float deltaWidth = (startWidth - startBounds.width()) / 2;
			startBounds.left -= deltaWidth;
			startBounds.right += deltaWidth;
		} else {
			// Extend start bounds vertically
			startScale = (float) startBounds.width() / finalBounds.width();
			float startHeight = startScale * finalBounds.height();
			float deltaHeight = (startHeight - startBounds.height()) / 2;
			startBounds.top -= deltaHeight;
			startBounds.bottom += deltaHeight;
		}
		Button b;
		// Hide the thumbnail and show the zoomed-in view. When the animation
		// begins,
		// it will position the zoomed-in view in the place of the thumbnail.
		// thumbView.setAlpha(0f);
		expandedImageView.setVisibility(View.VISIBLE);

		// Set the pivot point for SCALE_X and SCALE_Y transformations to the
		// top-left corner of
		// the zoomed-in view (the default is the center of the view).
		expandedImageView.setPivotX(0f);
		expandedImageView.setPivotY(0f);

		// Construct and run the parallel animation of the four translation and
		// scale properties
		// (X, Y, SCALE_X, and SCALE_Y).

		int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		System.out.println(currentapiVersion);
		if (currentapiVersion > android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {

			expandedImageView.setBackgroundColor(Color.WHITE);

			// Do something for froyo and above versions
			AnimatorSet set = new AnimatorSet();
			set.play(
					ObjectAnimator.ofFloat(expandedImageView, View.X,
							startBounds.left, finalBounds.left))
					.with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
							startBounds.top, finalBounds.top))
					.with(ObjectAnimator.ofFloat(expandedImageView,
							View.SCALE_X, startScale, 1f))
					.with(ObjectAnimator.ofFloat(expandedImageView,
							View.SCALE_Y, startScale, 1f))
					.with(ObjectAnimator.ofFloat(expandedImageView,
							expandedImageView.ALPHA, 0, 1));
			set.setDuration(mShortAnimationDuration);
			set.setInterpolator(new DecelerateInterpolator());
			set.addListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mCurrentAnimator = null;

				}

				@Override
				public void onAnimationCancel(Animator animation) {
					mCurrentAnimator = null;

				}
			});
			set.start();
			mCurrentAnimator = set;
		} else {
			System.out.println("non fare niente");
			// do something for phones running an SDK before froyo
		}

		// Upon clicking the zoomed-in image, it should zoom back down to the
		// original bounds
		// and show the thumbnail instead of the expanded image.
		final float startScaleFinal = startScale;
		expandedImageView.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {

				// regola.setVisibility(View.INVISIBLE);
				if (mCurrentAnimator != null) {
					mCurrentAnimator.cancel();

				}

				// Animate the four positioning/sizing properties in parallel,
				// back to their
				// original values.
				int currentapiVersion = android.os.Build.VERSION.SDK_INT;
				if (currentapiVersion > android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
					AnimatorSet set = new AnimatorSet();
					set.play(
							ObjectAnimator.ofFloat(expandedImageView, View.X,
									startBounds.left))
							.with(ObjectAnimator.ofFloat(expandedImageView,
									View.Y, startBounds.top))
							.with(ObjectAnimator.ofFloat(expandedImageView,
									View.SCALE_X, startScaleFinal))
							.with(ObjectAnimator.ofFloat(expandedImageView,
									View.SCALE_Y, startScaleFinal))
							.with(ObjectAnimator.ofFloat(expandedImageView,
									expandedImageView.ALPHA, 1, 0));
					set.setDuration(mShortAnimationDuration);
					set.setInterpolator(new DecelerateInterpolator());
					set.addListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {

							expandedImageView.setVisibility(View.GONE);
							ll.setVisibility(view1.VISIBLE);
							ll2.setVisibility(view1.INVISIBLE);
							mCurrentAnimator = null;
						}

						@Override
						public void onAnimationCancel(Animator animation) {
							thumbView.setAlpha(1f);
							expandedImageView.setVisibility(View.GONE);

							mCurrentAnimator = null;
						}
					});
					set.start();
					mCurrentAnimator = set;
				} else {

					expandedImageView.setVisibility(View.GONE);

				}
			}
		});
	}

}
