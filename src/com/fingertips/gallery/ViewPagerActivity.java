package com.fingertips.gallery;

import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fingertips.hinthunt.R;
import com.fingertips.hints.Quadro;
import com.fingertips.utils.ReadJSON;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;


public class ViewPagerActivity extends FragmentActivity {
	/** Called when the activity is first created. */

	private MyAdapter mAdapter;
	private ViewPager mPager;
	public ActionBar mActionBar;
	private Context ctx;
	JSONArray arrayJSON = null;
	static int NUM_ITEMS;
	public ArrayList<Quadro> itemList = new ArrayList<Quadro>();
	private int numStanza;
	String packName;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.maingallery);
		ctx = this.getApplicationContext();
		packName = getPackageName();
		Intent intent = getIntent();
		numStanza = intent.getIntExtra("position", 0);
		String lingua = Locale.getDefault().getDisplayLanguage();
		try {
			ReadJSON obj = new ReadJSON();
			
			if (lingua.equalsIgnoreCase("Italiano")) 
				arrayJSON = obj.leggiJSON(this, "stanze/stanza" + numStanza
						+ "/quadrigallery.json");
			
			if (lingua.equalsIgnoreCase("English")) {
				arrayJSON = obj.leggiJSON(this, "stanze/stanza" + numStanza
						+ "/quadrigallery_en.json");
			}
			
			
			JSONObject objectJSON;
			try {
				for (int i = 0; i < arrayJSON.length(); i++) {
					objectJSON = arrayJSON.getJSONObject(i);
					String uri = objectJSON.getString("thumb");
					int immagine = getResources().getIdentifier(uri, null,
							getPackageName());
					String descrizione = objectJSON.getString("opera");
					String dettagli = objectJSON.getString("dettagli");
					System.out.println("immagine: " + immagine
							+ " descrizione: " + descrizione);
					NUM_ITEMS = arrayJSON.length();
					Quadro q = new Quadro();
					q.setId(immagine);
					q.setOpera(descrizione);
					q.setDettagli(dettagli);

					itemList.add(i, q);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for (int i = 0; i < arrayJSON.length(); i++) {

			}
			mActionBar = getActionBar();
			mAdapter = new MyAdapter(getSupportFragmentManager(), itemList,
					ctx, numStanza, packName);
			mAdapter.setActionBar(mActionBar);
			mPager = (ViewPager) findViewById(R.id.pager);
			mPager.setAdapter(mAdapter);

			mPager.setOnPageChangeListener(new OnPageChangeListener() {

				public void onPageScrollStateChanged(int arg0) {
					// TODO Auto-generated method stub

				}

				public void onPageScrolled(int arg0, float arg1, int arg2) {
					// TODO Auto-generated method stub

				}

				public void onPageSelected(int arg0) {
					// TODO Auto-generated method stub
					Log.d("ViewPager", "onPageSelected: " + arg0);

					mActionBar.getTabAt(arg0).select();
				}

			});

			mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
			mActionBar.setDisplayShowTitleEnabled(false);
			Tab tab = mActionBar
					.newTab()
					.setText("1")
					.setTabListener(
							new TabListener<android.app.Fragment>(this, 0 + "",
									mPager));
			mActionBar.addTab(tab);
			for (int i = 2; i <= NUM_ITEMS; i++) {
				tab = mActionBar
						.newTab()
						.setText(Integer.toString(i))
						.setTabListener(
								new TabListener<android.app.Fragment>(this, i
										- 1 + "", mPager));
				mActionBar.addTab(tab);
			}

		} catch (Exception e) {
			Log.e("ViewPager", e.toString());
		}
	}

	public static class MyAdapter extends FragmentPagerAdapter {
		ActionBar mActionBar;
		ArrayList<Quadro> itemList;
		Context ctx;
		int numStanza;
		String packName;

		public MyAdapter(FragmentManager fm, ArrayList<Quadro> itemList,
				Context ctx, int numStanza, String packName) {
			super(fm);
			this.itemList = itemList;
			this.ctx = ctx;
			this.numStanza = numStanza;
			this.packName = packName;
		}

		@Override
		public int getCount() {
			return NUM_ITEMS;
		}

		@Override
		public android.support.v4.app.Fragment getItem(int position) {
			System.out.println("Position" + position);
			return new DetailFragment(itemList.get(position).getId(), itemList
					.get(position).getDettagli(), itemList.get(position)
					.getOpera(), position, ctx, numStanza, packName);

		}

		public void setActionBar(ActionBar bar) {
			mActionBar = bar;
		}
	}

}